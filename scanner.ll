%{

#include "scanner.hh"
#include <cstdlib>

#define YY_NO_UNISTD_H

using token = yy::Parser::token;

#undef  YY_DECL
#define YY_DECL int Scanner::yylex( yy::Parser::semantic_type * const lval, yy::Parser::location_type *loc )

/* update location on matching */
#define YY_USER_ACTION loc->step(); loc->columns(yyleng);

%}

%option c++
%option yyclass="Scanner"
%option noyywrap

%%

%{
    yylval = lval;
%}


[0-9]+		{
    yylval->build<int>(std::atoi(yytext));
    return token::NUMBER;
}

	 Commentaires
(--).*		{
    return token::COMMENTAIRE;
}

	Conditions
fin		{
    return token::FIN;
}
si		{
    return token::SI;
}
sinon\:		{
    return token::SINON;
}
repete		{
    return token::REPETE;
}
tant\ que	{
    return token::TANT_QUE;
}
pas		{
    return token::PAS;
}
mur		{
    return token::MUR;		// mur <direction>
}
vide		{
    return token::VIDE;		// vide <direction>
}
à\ gauche\:	{
    return token::C_GAUCHE;
}
à\ droite\:	{
    return token::C_DROITE;
}
devant\:	{
    return token::C_DEVANT;
}
derriere\:	{
    return token::C_DERRIERE;
}
fois\:		{
    return token::C_FOIS;
}
\@[0-9]+\:	{
    yylval->build<std::string>(yytext);
    return token::C_N_TURTLE;
}


	Declarations
fonction	{
    return token::FONCTION;
}
	
  Instructions
avance		{
    return token::AVANCE;  	// avance [nombre]	(1 case)
}
recule		{
    return token::RECULE;	// recule [nombre]	(1 case)
}
saute		{
    return token::SAUTE;	// saute [nombre]	(2 cases)
}
tourne		{
    return token::TOURNE; 	// tourne <à gauche|à droite > [nombre de fois]
}
couleur		{
    return token::COULEUR;	// couleur <carapace|motif> #aBcDeF
}
tortues		{
    return token::TORTUES;	// tortues <nombre>
}
jardin		{
    return token::JARDIN;	// jardin '<fichier>'
}
fois		{
    return token::FOIS;
}
\@[0-9]+	{
    yylval->build<std::string>(yytext);
    return token::N_TURTLE;
}

	Directions
à\ gauche       {
    return token::GAUCHE;
}
à\ droite       {
    return token::DROITE;
}
devant       {
	return token::DEVANT;
}
derriere       {
	return token::DERRIERE;
}

  Operations
"\="          {
    return token::EQU;
}
"\+"          {
    return token::ADD;
}
"\-"          {
    return token::NEG;
}
"\/"          {
    return token::DIV;
}
"\*"          {
    return token::MUL;
}
"\("          {
    return token::PO;
}
"\)"          {
    return token::PC;
}

"\<"          {
    return token::LT;
}
"\>"          {
    return token::GT;
}
"\<\="        {
    return token::LET;
}
"\>\="        {
    return token::GET;
}
"\=\="        {
    return token::EQ;
}
"\!\="        {
    return token::NEQ;
}
"\&\&"        {
    return token::AND;
}
"\|\|"        {
    return token::OR;
}
(XOR)         {
    return token::XOR;
}
"\!"          {
    return token::NOT;
}

\'(.*)\'	{
    yylval->build<std::string>(yytext);
    return token::FICHIER;
}
\#[a-zA-Z0-9]{6}	{
    yylval->build<std::string>(yytext);
    return token::HEX_COLOR;
}
[a-zA-Z]+\:	{
    yylval->build<std::string>(yytext);
    return token::NAME;
}

"\n"		{
    loc->lines();
    return token::NL;
}

.           {}

%%
