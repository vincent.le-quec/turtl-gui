//
// driver.cc for  in /home/venatum/rendu/L3-Angers/Compil/Projet
//
// Made by Venatum
// Login   <vincentlequec@gmail.com>
//
// Started on  Wed Nov 28 13:05:42 2018 Venatum
// Last update Sun Dec 16 16:43:08 2018 Venatum
//

#include "driver.hh"
#include "jardinRendering.hh"
#include <iostream>

Driver::Driver(JardinRendering * J) {
    monJardin = J;
}

Driver::~Driver() {
	delete monJardin;
}

// TODO:
// Poser/Lever Stylo -> pb tracage
// creer un jardin -> osef... ou presque
// Clean Code
static const t_pointer	func[] = {
	{"addTurtles",		&Driver::addTurtles},
	{"nAvanceN",		&Driver::nAvanceN},
	{"nReculeN",		&Driver::nReculeN},
	{"nSauteN",			&Driver::nSauteN},
	{"nTourneN",		&Driver::nTourneN},
	{"nSetCarapace",	&Driver::nSetCarapace},
	{"nSetMotif",		&Driver::nSetMotif},
	{"estMur",			&Driver::estMur},
	{"estVide",			&Driver::estVide},
	{"allAvanceN",		&Driver::allAvanceN},
	{"allReculeN",		&Driver::allReculeN},
	{"allSauteN",		&Driver::allSauteN},
	{"allTourneN",		&Driver::allTourneN},
	{"allSetCarapace",	&Driver::allSetCarapace},
	{"allSetMotif",		&Driver::allSetMotif},
	{"allEstMur",		&Driver::allEstMur},
	{"allEstVide",		&Driver::allEstVide},
	{"repete",			&Driver::myRepete},
	{"tantQue",			&Driver::myTantQue},
	{"si",				&Driver::myIf},
//	{"sinon",			nullptr},
//	{"finRepete",		nullptr},
//	{"finTantQue",		nullptr},
//	{"finSi",			nullptr},
};
static size_t it = 0;

void	Driver::empile		(const std::string &str, t_turtle t) {
	pileFunc.push_back(str);
	pileArg.push_back(t);
}
void	Driver::depile		() {
	pileFunc.pop_back();
	pileArg.pop_back();
}
void	Driver::exec		() {
	Color::Modifier green(Color::FG_GREEN);
	Color::Modifier def(Color::FG_DEFAULT);
	std::clog << green << "[INFO] " << def << "Affichage sur clog: driver.cc - ligne 67 & 75\n";
//	std::clog << "Pile: " << pileFunc.size() << "\n";
	for (; it < pileFunc.size(); it++)
		execFunction(it);
}

void	Driver::execFunction(int j) {
	int i = 0;

//	std::clog << "[" s<< j << "]\t" <<pileFunc[j] << "\n";

	while (func[i].pointer) {
		if (pileFunc[j] == func[i].str) {
			(this->*func[i].pointer)(pileArg[j]);
			break;
		}
		i++;
	}
}

void	Driver::myIf		(t_turtle &t){
	if (pileArg[it].condition) {
		it++; // si
		while (pileFunc[it] != "finSi" && pileFunc[it] != "sinon")
			execFunction(it++);
		if (pileFunc[it] == "sinon") {
			it++; // sinon
			while (pileFunc[it] != "finSi")
				it++; // skip
		}
	} else {
		while (pileFunc[it] != "finSi" && pileFunc[it] != "sinon")
			it++; // skip
		if (pileFunc[it] == "sinon")
			while (pileFunc[it] != "finSi") {
				execFunction(it);
				it++;
			}
	}
}
void	Driver::myTantQue	(t_turtle &t){
	int save, i = 0;
	bool condition;

	save = ++it; // 1ere instruction
	// Find condition
	while (func[i++].pointer)
		if (pileFunc[save - 2] == func[i].str)
			break;
	// Negation
	condition = pileArg[save - 2].condition;
	if (pileArg[save - 1].negation)
		condition = !condition;
	// Exec tq
	while (condition) {
		it = save; // reset boucle
		while (pileFunc[it] != "finTantque") {
			execFunction(it);
			it++;
		}
		// Set condition
		(this->*func[i].pointer)(pileArg[save - 2]);
		condition = pileArg[save - 2].condition;
		if (pileArg[save - 1].negation)
			condition = !condition;
	}
}
void	Driver::myRepete	(t_turtle &t){
	int save;

	it++;
	save = it; // 1ere instruction
	for (int i = 0; i < pileArg[save - 1].n; ++i) {
		it = save; // Reset boucle
		while (pileFunc[it] != "finRepete") {
			execFunction(it);
			it++;
		}
	}
}

static const std::vector<direction> tabDir = {direction::north, direction::east, direction::south, direction::west};

void	Driver::myError(const std::string &str) {
	Color::Modifier red(Color::FG_RED);
	Color::Modifier def(Color::FG_DEFAULT);
	std::cerr << red << "[ERREUR] " << def << str << std::endl;
}
int		Driver::getTurtle(const std::string &str) {
	std::string tmp(str);
	unsigned int turtle = std::stoi(tmp.erase(0, 1));

	if (turtle > monJardin->nombreTortues())
		myError("La tortue " + str + " n'existe pas.");
	return turtle;
}
bool	Driver::outOfMap(int x, int y) {
	return !((x < monJardin->tailleJardin().width() && x >= 0) &&
			(y < monJardin->tailleJardin().height() && y >= 0));
}

void	Driver::loadJardin	(t_turtle &t) {
	Color::Modifier green(Color::FG_GREEN);
	Color::Modifier def(Color::FG_DEFAULT);

	std::clog << green << "[LOADING] " << def << t.path << std::endl;
//	monJardin = monJardin(t.path);
}
void	Driver::addTurtles	(t_turtle &t) {
	for (int i = 0; i < t.n; ++i)
		monJardin->nouvelleTortue();
}
void	Driver::nAvanceN	(t_turtle &t) {
	float	ori = monJardin->orientation(t.turtle);
	QPoint	tmp;
	int		x, y;

	for (int i = 0; i < t.step; ++i) {
		tmp = monJardin->position(t.turtle);
		x = tmp.x();
		y = tmp.y();
		if (ori == direction::north)
			y -= 1;
		else if (ori == direction::east)
			x += 1;
		else if (ori == direction::south)
			y += 1;
		else if (ori == direction::west)
			x -= 1;

//		std::cout << "X = " << x << "\tY = " << y << "\n";
		if (monJardin->estMur(x, y) || monJardin->estTortue(x, y)) {
			myError("Avance " + std::to_string(t.step) + " @" + std::to_string(t.turtle) + " - Il y a deja un element a cette position.");
			break;
		} else if (outOfMap(x, y)) {
			myError("Avance " + std::to_string(t.step) + " @" + std::to_string(t.turtle) + " - Hors de map");
			break;
		} else {
			 monJardin->poserStylo(t.turtle);
			monJardin->changePosition(t.turtle, x, y);
			 monJardin->leverStylo(t.turtle);
		}
	}
}
void	Driver::nReculeN	(t_turtle &t) {
	float	ori = monJardin->orientation(t.turtle);
	QPoint	tmp;
	int		x, y;

	for (int i = 0; i < t.step; ++i) {
		tmp = monJardin->position(t.turtle);
		x = tmp.x();
		y = tmp.y();
		if (ori == direction::north)
			y += 1;
		else if (ori == direction::east)
			x -= 1;
		else if (ori == direction::south)
			y -= 1;
		else if (ori == direction::west)
			x += 1;

		if (monJardin->estMur(x, y) || monJardin->estTortue(x, y)) {
			myError("Recule " + std::to_string(t.step) + " @" + std::to_string(t.turtle) + " - Il y a deja un element a cette position.");
			break;
		} else if (outOfMap(x, y)) {
			myError("Recule" + std::to_string(t.step) + " @" + std::to_string(t.turtle) + " - Hors de map");
			break;
		} else {
			monJardin->poserStylo(t.turtle);
			monJardin->changePosition(t.turtle, x, y);
			monJardin->leverStylo(t.turtle);
		}
	}
}
void	Driver::nSauteN		(t_turtle &t) {
	float	ori = monJardin->orientation(t.turtle);
	QPoint	tmp;
	int		x, y;
	for (int i = 0; i < t.step; ++i) {
		tmp = monJardin->position(t.turtle);
		x = tmp.x();
		y = tmp.y();
		if (ori == direction::north)
			y -= 2;
		else if (ori == direction::east)
			x += 2;
		else if (ori == direction::south)
			y += 2;
		else if (ori == direction::west)
			x -= 2;

		if (monJardin->estMur(x, y) || monJardin->estTortue(x, y)) {
			myError("Saute " + std::to_string(t.step) + " @" + std::to_string(t.turtle) + " - Il y a deja un element a cette position.");
			break;
		} else if (outOfMap(x, y)) {
			myError("Saute " + std::to_string(t.step) + " @" + std::to_string(t.turtle) + " - Hors de map");
			break;
		} else
			monJardin->changePosition(t.turtle, x, y);
	}
}
void	Driver::nTourneN	(t_turtle &t) {
	auto it(std::find(tabDir.begin(), tabDir.end(), monJardin->orientation(t.turtle)));

	for (int i = 0; i < t.n; ++i) {
		if (t.tourne == 'd') {
			if (it == tabDir.end())
				it = tabDir.begin();
			else
				it++;
		} else {
			if (it == tabDir.begin())
				it = tabDir.end();
			else
				it--;
		}
	}
	monJardin->changeOrientation(t.turtle, *it);
}
void	Driver::nSetCarapace(t_turtle &t) {
	// https://stackoverflow.com/questions/22508673/from-hexadecimal-string-color-to-an-rgb-color
	std::vector<std::string> colori;

	colori.push_back(t.color.substr(1, 2));
	colori.push_back(t.color.substr(3, 2));
	colori.push_back(t.color.substr(5, 2));

	int r = std::stoi(colori[0], nullptr, 16);
	int	g = std::stoi(colori[1], nullptr, 16);
	int	b = std::stoi(colori[2], nullptr, 16);

	monJardin->changeCouleurCarapace(t.turtle, r, g, b);
}
void	Driver::nSetMotif	(t_turtle &t) {
	std::vector<std::string> colori;

	colori.push_back(t.color.substr(1, 2));
	colori.push_back(t.color.substr(3, 2));
	colori.push_back(t.color.substr(5, 2));

	int r = std::stoi(colori[0], nullptr, 16);
	int	g = std::stoi(colori[1], nullptr, 16);
	int	b = std::stoi(colori[2], nullptr, 16);

	monJardin->changeCouleurMotif(t.turtle, r, g, b);
}
void	Driver::estMur		(t_turtle &t) {
	int		x = monJardin->getTortues().at(t.turtle)->getX();
	int		y = monJardin->getTortues().at(t.turtle)->getY();
	float	ori = monJardin->orientation(t.turtle);
	bool	res = false;

	if (t.d == direction::north) {			// Devant
		if (ori == direction::north)
			res = monJardin->estMur(x, y - 1) || outOfMap(x, y - 1);
		else if (ori == direction::east)
			res = monJardin->estMur(x + 1, y) || outOfMap(x + 1, y);
		else if (ori == direction::south)
			res = monJardin->estMur(x, y + 1) || outOfMap(x, y + 1);
		else if (ori == direction::west)
			res = monJardin->estMur(x - 1, y) || outOfMap(x - 1, y);
	} else if (t.d == direction::east) {		// Droite
		if (ori == direction::north)
			res = monJardin->estMur(x + 1, y) || outOfMap(x + 1, y);
		else if (ori == direction::east)
			res = monJardin->estMur(x, y + 1) || outOfMap(x, y + 1);
		else if (ori == direction::south)
			res = monJardin->estMur(x - 1, y) || outOfMap(x - 1, y);
		else if (ori == direction::west)
			res = monJardin->estMur(x, y - 1) || outOfMap(x, y - 1);
	} else if (t.d == direction::south) {		// Derriere
		if (ori == direction::north)
			res = monJardin->estMur(x, y + 1) || outOfMap(x, y + 1);
		else if (ori == direction::east)
			res = monJardin->estMur(x - 1, y) || outOfMap(x - 1, y);
		else if (ori == direction::south)
			res = monJardin->estMur(x, y - 1) || outOfMap(x, y - 1);
		else if (ori == direction::west)
			res = monJardin->estMur(x + 1, y) || outOfMap(x + 1, y);
	} else if (t.d == direction::west) {		// Gauche
		if (ori == direction::north)
			res = monJardin->estMur(x - 1, y) || outOfMap(x - 1, y);
		else if (ori == direction::east)
			res = monJardin->estMur(x, y - 1) || outOfMap(x, y - 1);
		else if (ori == direction::south)
			res = monJardin->estMur(x + 1, y) || outOfMap(x + 1, y);
		else if (ori == direction::west)
			res = monJardin->estMur(x, y + 1) || outOfMap(x, y + 1);
	}
	t.condition = res;
}
void	Driver::estVide		(t_turtle &t) {
	int		x = monJardin->getTortues().at(t.turtle)->getX();
	int		y = monJardin->getTortues().at(t.turtle)->getY();
	float	ori = monJardin->orientation(t.turtle);
	estMur(t);
	bool	res = t.condition;

	if (res) {
		t.condition = false;
		return ;
	}

	if (t.d == direction::north) {				// Devant
		if (ori == direction::north)
			res = monJardin->estTortue(x, y - 1) || outOfMap(x, y - 1);
		else if (ori == direction::east)
			res = monJardin->estTortue(x + 1, y) || outOfMap(x + 1, y);
		else if (ori == direction::south)
			res = monJardin->estTortue(x, y + 1) || outOfMap(x, y + 1);
		else if (ori == direction::west)
			res = monJardin->estTortue(x - 1, y) || outOfMap(x - 1, y);
	} else if (t.d == direction::east) {		// Droite
		if (ori == direction::north)
			res = monJardin->estTortue(x + 1, y) || outOfMap(x + 1, y);
		else if (ori == direction::east)
			res = monJardin->estTortue(x, y + 1) || outOfMap(x, y + 1);
		else if (ori == direction::south)
			res = monJardin->estTortue(x - 1, y) || outOfMap(x - 1, y);
		else if (ori == direction::west)
			res = monJardin->estTortue(x, y - 1) || outOfMap(x, y - 1);
	} else if (t.d == direction::south) {		// Derriere
		if (ori == direction::north)
			res = monJardin->estTortue(x, y + 1) || outOfMap(x, y + 1);
		else if (ori == direction::east)
			res = monJardin->estTortue(x - 1, y) || outOfMap(x - 1, y);
		else if (ori == direction::south)
			res = monJardin->estTortue(x, y - 1) || outOfMap(x, y - 1);
		else if (ori == direction::west)
			res = monJardin->estTortue(x + 1, y) || outOfMap(x + 1, y);
	} else if (t.d == direction::west) {		// Gauche
		if (ori == direction::north)
			res = monJardin->estTortue(x - 1, y) || outOfMap(x - 1, y);
		else if (ori == direction::east)
			res = monJardin->estTortue(x, y - 1) || outOfMap(x, y - 1);
		else if (ori == direction::south)
			res = monJardin->estTortue(x + 1, y) || outOfMap(x + 1, y);
		else if (ori == direction::west)
			res = monJardin->estTortue(x, y + 1) || outOfMap(x, y + 1);
	}
	t.condition = !res;
}

void	Driver::allAvanceN	(t_turtle &t) {
	for (unsigned int i = 0; i < monJardin->nombreTortues(); i++) {
		t.turtle = i;
		nAvanceN(t);
	}
}
void	Driver::allReculeN	(t_turtle &t) {
	for (unsigned int i = 0; i < monJardin->nombreTortues(); i++) {
		t.turtle = i;
		nReculeN(t);
	}
}
void	Driver::allSauteN	(t_turtle &t) {
	for (unsigned int i = 0; i < monJardin->nombreTortues(); i++) {
		t.turtle = i;
		nSauteN(t);
	}
}
void	Driver::allTourneN	(t_turtle &t) {
	for (unsigned int i = 0; i < monJardin->nombreTortues(); i++) {
		t.turtle = i;
		nTourneN(t);
	}
}
void	Driver::allSetCarapace(t_turtle &t) {
	for (unsigned int i = 0; i < monJardin->nombreTortues(); i++) {
		t.turtle = i;
		nSetCarapace(t);
	}
}
void	Driver::allSetMotif	(t_turtle &t) {
	for (unsigned int i = 0; i < monJardin->nombreTortues(); i++) {
		t.turtle = i;
		nSetMotif(t);
	}
}
void	Driver::allEstMur	(t_turtle &t) {
	bool res = true;

	for (unsigned int i = 0; i < monJardin->nombreTortues(); i++) {
		t.turtle = i;
		estMur(t);
		res = (res && t.condition);
		if (!res) break;
	}
	t.condition = res;
}
void	Driver::allEstVide	(t_turtle &t) {
	bool res = true;

	for (unsigned int i = 0; i < monJardin->nombreTortues(); i++) {
		t.turtle = i;
		estVide(t);
		res = (res && t.condition);
		if (!res) break;
	}
	t.condition = res;
}
