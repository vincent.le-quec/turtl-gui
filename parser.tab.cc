// A Bison parser, made by GNU Bison 3.0.4.

// Skeleton implementation for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.


// First part of user declarations.

#line 37 "parser.tab.cc" // lalr1.cc:404

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

#include "parser.tab.hh"

// User implementation prologue.

#line 51 "parser.tab.cc" // lalr1.cc:412
// Unqualified %code blocks.
#line 19 "parser.yy" // lalr1.cc:413

    #include <iostream>
    #include <string>
    
    #include "scanner.hh"
    #include "driver.hh"

    #undef  yylex
    #define yylex scanner.yylex

#line 64 "parser.tab.cc" // lalr1.cc:413


#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> // FIXME: INFRINGES ON USER NAME SPACE.
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K].location)
/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

# ifndef YYLLOC_DEFAULT
#  define YYLLOC_DEFAULT(Current, Rhs, N)                               \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).begin  = YYRHSLOC (Rhs, 1).begin;                   \
          (Current).end    = YYRHSLOC (Rhs, N).end;                     \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).begin = (Current).end = YYRHSLOC (Rhs, 0).end;      \
        }                                                               \
    while (/*CONSTCOND*/ false)
# endif


// Suppress unused-variable warnings by "using" E.
#define YYUSE(E) ((void) (E))

// Enable debugging if requested.
#if YYDEBUG

// A pseudo ostream that takes yydebug_ into account.
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Symbol)         \
  do {                                          \
    if (yydebug_)                               \
    {                                           \
      *yycdebug_ << Title << ' ';               \
      yy_print_ (*yycdebug_, Symbol);           \
      *yycdebug_ << std::endl;                  \
    }                                           \
  } while (false)

# define YY_REDUCE_PRINT(Rule)          \
  do {                                  \
    if (yydebug_)                       \
      yy_reduce_print_ (Rule);          \
  } while (false)

# define YY_STACK_PRINT()               \
  do {                                  \
    if (yydebug_)                       \
      yystack_print_ ();                \
  } while (false)

#else // !YYDEBUG

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Symbol)  YYUSE(Symbol)
# define YY_REDUCE_PRINT(Rule)           static_cast<void>(0)
# define YY_STACK_PRINT()                static_cast<void>(0)

#endif // !YYDEBUG

#define yyerrok         (yyerrstatus_ = 0)
#define yyclearin       (yyla.clear ())

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)


namespace yy {
#line 150 "parser.tab.cc" // lalr1.cc:479

  /// Build a parser object.
   Parser :: Parser  (Scanner &scanner_yyarg, Driver &driver_yyarg)
    :
#if YYDEBUG
      yydebug_ (false),
      yycdebug_ (&std::cerr),
#endif
      scanner (scanner_yyarg),
      driver (driver_yyarg)
  {}

   Parser ::~ Parser  ()
  {}


  /*---------------.
  | Symbol types.  |
  `---------------*/

  inline
   Parser ::syntax_error::syntax_error (const location_type& l, const std::string& m)
    : std::runtime_error (m)
    , location (l)
  {}

  // basic_symbol.
  template <typename Base>
  inline
   Parser ::basic_symbol<Base>::basic_symbol ()
    : value ()
  {}

  template <typename Base>
  inline
   Parser ::basic_symbol<Base>::basic_symbol (const basic_symbol& other)
    : Base (other)
    , value ()
    , location (other.location)
  {
      switch (other.type_get ())
    {
      case 66: // condition
        value.copy< bool > (other.value);
        break;

      case 47: // REAL
        value.copy< float > (other.value);
        break;

      case 46: // NUMBER
      case 70: // operation
      case 71: // nombre
        value.copy< int > (other.value);
        break;

      case 48: // NAME
      case 49: // FICHIER
      case 50: // HEX_COLOR
      case 51: // N_TURTLE
      case 52: // C_N_TURTLE
        value.copy< std::string > (other.value);
        break;

      default:
        break;
    }

  }


  template <typename Base>
  inline
   Parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const semantic_type& v, const location_type& l)
    : Base (t)
    , value ()
    , location (l)
  {
    (void) v;
      switch (this->type_get ())
    {
      case 66: // condition
        value.copy< bool > (v);
        break;

      case 47: // REAL
        value.copy< float > (v);
        break;

      case 46: // NUMBER
      case 70: // operation
      case 71: // nombre
        value.copy< int > (v);
        break;

      case 48: // NAME
      case 49: // FICHIER
      case 50: // HEX_COLOR
      case 51: // N_TURTLE
      case 52: // C_N_TURTLE
        value.copy< std::string > (v);
        break;

      default:
        break;
    }
}


  // Implementation of basic_symbol constructor for each type.

  template <typename Base>
   Parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const location_type& l)
    : Base (t)
    , value ()
    , location (l)
  {}

  template <typename Base>
   Parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const bool v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   Parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const float v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   Parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const int v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
   Parser ::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const std::string v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}


  template <typename Base>
  inline
   Parser ::basic_symbol<Base>::~basic_symbol ()
  {
    clear ();
  }

  template <typename Base>
  inline
  void
   Parser ::basic_symbol<Base>::clear ()
  {
    // User destructor.
    symbol_number_type yytype = this->type_get ();
    basic_symbol<Base>& yysym = *this;
    (void) yysym;
    switch (yytype)
    {
   default:
      break;
    }

    // Type destructor.
    switch (yytype)
    {
      case 66: // condition
        value.template destroy< bool > ();
        break;

      case 47: // REAL
        value.template destroy< float > ();
        break;

      case 46: // NUMBER
      case 70: // operation
      case 71: // nombre
        value.template destroy< int > ();
        break;

      case 48: // NAME
      case 49: // FICHIER
      case 50: // HEX_COLOR
      case 51: // N_TURTLE
      case 52: // C_N_TURTLE
        value.template destroy< std::string > ();
        break;

      default:
        break;
    }

    Base::clear ();
  }

  template <typename Base>
  inline
  bool
   Parser ::basic_symbol<Base>::empty () const
  {
    return Base::type_get () == empty_symbol;
  }

  template <typename Base>
  inline
  void
   Parser ::basic_symbol<Base>::move (basic_symbol& s)
  {
    super_type::move(s);
      switch (this->type_get ())
    {
      case 66: // condition
        value.move< bool > (s.value);
        break;

      case 47: // REAL
        value.move< float > (s.value);
        break;

      case 46: // NUMBER
      case 70: // operation
      case 71: // nombre
        value.move< int > (s.value);
        break;

      case 48: // NAME
      case 49: // FICHIER
      case 50: // HEX_COLOR
      case 51: // N_TURTLE
      case 52: // C_N_TURTLE
        value.move< std::string > (s.value);
        break;

      default:
        break;
    }

    location = s.location;
  }

  // by_type.
  inline
   Parser ::by_type::by_type ()
    : type (empty_symbol)
  {}

  inline
   Parser ::by_type::by_type (const by_type& other)
    : type (other.type)
  {}

  inline
   Parser ::by_type::by_type (token_type t)
    : type (yytranslate_ (t))
  {}

  inline
  void
   Parser ::by_type::clear ()
  {
    type = empty_symbol;
  }

  inline
  void
   Parser ::by_type::move (by_type& that)
  {
    type = that.type;
    that.clear ();
  }

  inline
  int
   Parser ::by_type::type_get () const
  {
    return type;
  }
  // Implementation of make_symbol for each symbol type.
   Parser ::symbol_type
   Parser ::make_NL (const location_type& l)
  {
    return symbol_type (token::NL, l);
  }

   Parser ::symbol_type
   Parser ::make_ADD (const location_type& l)
  {
    return symbol_type (token::ADD, l);
  }

   Parser ::symbol_type
   Parser ::make_NEG (const location_type& l)
  {
    return symbol_type (token::NEG, l);
  }

   Parser ::symbol_type
   Parser ::make_MUL (const location_type& l)
  {
    return symbol_type (token::MUL, l);
  }

   Parser ::symbol_type
   Parser ::make_DIV (const location_type& l)
  {
    return symbol_type (token::DIV, l);
  }

   Parser ::symbol_type
   Parser ::make_PO (const location_type& l)
  {
    return symbol_type (token::PO, l);
  }

   Parser ::symbol_type
   Parser ::make_PC (const location_type& l)
  {
    return symbol_type (token::PC, l);
  }

   Parser ::symbol_type
   Parser ::make_EQU (const location_type& l)
  {
    return symbol_type (token::EQU, l);
  }

   Parser ::symbol_type
   Parser ::make_LT (const location_type& l)
  {
    return symbol_type (token::LT, l);
  }

   Parser ::symbol_type
   Parser ::make_GT (const location_type& l)
  {
    return symbol_type (token::GT, l);
  }

   Parser ::symbol_type
   Parser ::make_LET (const location_type& l)
  {
    return symbol_type (token::LET, l);
  }

   Parser ::symbol_type
   Parser ::make_GET (const location_type& l)
  {
    return symbol_type (token::GET, l);
  }

   Parser ::symbol_type
   Parser ::make_EQ (const location_type& l)
  {
    return symbol_type (token::EQ, l);
  }

   Parser ::symbol_type
   Parser ::make_NEQ (const location_type& l)
  {
    return symbol_type (token::NEQ, l);
  }

   Parser ::symbol_type
   Parser ::make_AND (const location_type& l)
  {
    return symbol_type (token::AND, l);
  }

   Parser ::symbol_type
   Parser ::make_OR (const location_type& l)
  {
    return symbol_type (token::OR, l);
  }

   Parser ::symbol_type
   Parser ::make_XOR (const location_type& l)
  {
    return symbol_type (token::XOR, l);
  }

   Parser ::symbol_type
   Parser ::make_NOT (const location_type& l)
  {
    return symbol_type (token::NOT, l);
  }

   Parser ::symbol_type
   Parser ::make_COMMENTAIRE (const location_type& l)
  {
    return symbol_type (token::COMMENTAIRE, l);
  }

   Parser ::symbol_type
   Parser ::make_FIN (const location_type& l)
  {
    return symbol_type (token::FIN, l);
  }

   Parser ::symbol_type
   Parser ::make_SI (const location_type& l)
  {
    return symbol_type (token::SI, l);
  }

   Parser ::symbol_type
   Parser ::make_SINON (const location_type& l)
  {
    return symbol_type (token::SINON, l);
  }

   Parser ::symbol_type
   Parser ::make_REPETE (const location_type& l)
  {
    return symbol_type (token::REPETE, l);
  }

   Parser ::symbol_type
   Parser ::make_TANT_QUE (const location_type& l)
  {
    return symbol_type (token::TANT_QUE, l);
  }

   Parser ::symbol_type
   Parser ::make_FONCTION (const location_type& l)
  {
    return symbol_type (token::FONCTION, l);
  }

   Parser ::symbol_type
   Parser ::make_AVANCE (const location_type& l)
  {
    return symbol_type (token::AVANCE, l);
  }

   Parser ::symbol_type
   Parser ::make_RECULE (const location_type& l)
  {
    return symbol_type (token::RECULE, l);
  }

   Parser ::symbol_type
   Parser ::make_SAUTE (const location_type& l)
  {
    return symbol_type (token::SAUTE, l);
  }

   Parser ::symbol_type
   Parser ::make_TOURNE (const location_type& l)
  {
    return symbol_type (token::TOURNE, l);
  }

   Parser ::symbol_type
   Parser ::make_COULEUR (const location_type& l)
  {
    return symbol_type (token::COULEUR, l);
  }

   Parser ::symbol_type
   Parser ::make_TORTUES (const location_type& l)
  {
    return symbol_type (token::TORTUES, l);
  }

   Parser ::symbol_type
   Parser ::make_JARDIN (const location_type& l)
  {
    return symbol_type (token::JARDIN, l);
  }

   Parser ::symbol_type
   Parser ::make_FOIS (const location_type& l)
  {
    return symbol_type (token::FOIS, l);
  }

   Parser ::symbol_type
   Parser ::make_GAUCHE (const location_type& l)
  {
    return symbol_type (token::GAUCHE, l);
  }

   Parser ::symbol_type
   Parser ::make_DROITE (const location_type& l)
  {
    return symbol_type (token::DROITE, l);
  }

   Parser ::symbol_type
   Parser ::make_PAS (const location_type& l)
  {
    return symbol_type (token::PAS, l);
  }

   Parser ::symbol_type
   Parser ::make_MUR (const location_type& l)
  {
    return symbol_type (token::MUR, l);
  }

   Parser ::symbol_type
   Parser ::make_VIDE (const location_type& l)
  {
    return symbol_type (token::VIDE, l);
  }

   Parser ::symbol_type
   Parser ::make_C_GAUCHE (const location_type& l)
  {
    return symbol_type (token::C_GAUCHE, l);
  }

   Parser ::symbol_type
   Parser ::make_C_DROITE (const location_type& l)
  {
    return symbol_type (token::C_DROITE, l);
  }

   Parser ::symbol_type
   Parser ::make_C_DEVANT (const location_type& l)
  {
    return symbol_type (token::C_DEVANT, l);
  }

   Parser ::symbol_type
   Parser ::make_C_DERRIERE (const location_type& l)
  {
    return symbol_type (token::C_DERRIERE, l);
  }

   Parser ::symbol_type
   Parser ::make_C_FOIS (const location_type& l)
  {
    return symbol_type (token::C_FOIS, l);
  }

   Parser ::symbol_type
   Parser ::make_NUMBER (const int& v, const location_type& l)
  {
    return symbol_type (token::NUMBER, v, l);
  }

   Parser ::symbol_type
   Parser ::make_REAL (const float& v, const location_type& l)
  {
    return symbol_type (token::REAL, v, l);
  }

   Parser ::symbol_type
   Parser ::make_NAME (const std::string& v, const location_type& l)
  {
    return symbol_type (token::NAME, v, l);
  }

   Parser ::symbol_type
   Parser ::make_FICHIER (const std::string& v, const location_type& l)
  {
    return symbol_type (token::FICHIER, v, l);
  }

   Parser ::symbol_type
   Parser ::make_HEX_COLOR (const std::string& v, const location_type& l)
  {
    return symbol_type (token::HEX_COLOR, v, l);
  }

   Parser ::symbol_type
   Parser ::make_N_TURTLE (const std::string& v, const location_type& l)
  {
    return symbol_type (token::N_TURTLE, v, l);
  }

   Parser ::symbol_type
   Parser ::make_C_N_TURTLE (const std::string& v, const location_type& l)
  {
    return symbol_type (token::C_N_TURTLE, v, l);
  }



  // by_state.
  inline
   Parser ::by_state::by_state ()
    : state (empty_state)
  {}

  inline
   Parser ::by_state::by_state (const by_state& other)
    : state (other.state)
  {}

  inline
  void
   Parser ::by_state::clear ()
  {
    state = empty_state;
  }

  inline
  void
   Parser ::by_state::move (by_state& that)
  {
    state = that.state;
    that.clear ();
  }

  inline
   Parser ::by_state::by_state (state_type s)
    : state (s)
  {}

  inline
   Parser ::symbol_number_type
   Parser ::by_state::type_get () const
  {
    if (state == empty_state)
      return empty_symbol;
    else
      return yystos_[state];
  }

  inline
   Parser ::stack_symbol_type::stack_symbol_type ()
  {}


  inline
   Parser ::stack_symbol_type::stack_symbol_type (state_type s, symbol_type& that)
    : super_type (s, that.location)
  {
      switch (that.type_get ())
    {
      case 66: // condition
        value.move< bool > (that.value);
        break;

      case 47: // REAL
        value.move< float > (that.value);
        break;

      case 46: // NUMBER
      case 70: // operation
      case 71: // nombre
        value.move< int > (that.value);
        break;

      case 48: // NAME
      case 49: // FICHIER
      case 50: // HEX_COLOR
      case 51: // N_TURTLE
      case 52: // C_N_TURTLE
        value.move< std::string > (that.value);
        break;

      default:
        break;
    }

    // that is emptied.
    that.type = empty_symbol;
  }

  inline
   Parser ::stack_symbol_type&
   Parser ::stack_symbol_type::operator= (const stack_symbol_type& that)
  {
    state = that.state;
      switch (that.type_get ())
    {
      case 66: // condition
        value.copy< bool > (that.value);
        break;

      case 47: // REAL
        value.copy< float > (that.value);
        break;

      case 46: // NUMBER
      case 70: // operation
      case 71: // nombre
        value.copy< int > (that.value);
        break;

      case 48: // NAME
      case 49: // FICHIER
      case 50: // HEX_COLOR
      case 51: // N_TURTLE
      case 52: // C_N_TURTLE
        value.copy< std::string > (that.value);
        break;

      default:
        break;
    }

    location = that.location;
    return *this;
  }


  template <typename Base>
  inline
  void
   Parser ::yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const
  {
    if (yymsg)
      YY_SYMBOL_PRINT (yymsg, yysym);
  }

#if YYDEBUG
  template <typename Base>
  void
   Parser ::yy_print_ (std::ostream& yyo,
                                     const basic_symbol<Base>& yysym) const
  {
    std::ostream& yyoutput = yyo;
    YYUSE (yyoutput);
    symbol_number_type yytype = yysym.type_get ();
    // Avoid a (spurious) G++ 4.8 warning about "array subscript is
    // below array bounds".
    if (yysym.empty ())
      std::abort ();
    yyo << (yytype < yyntokens_ ? "token" : "nterm")
        << ' ' << yytname_[yytype] << " ("
        << yysym.location << ": ";
    YYUSE (yytype);
    yyo << ')';
  }
#endif

  inline
  void
   Parser ::yypush_ (const char* m, state_type s, symbol_type& sym)
  {
    stack_symbol_type t (s, sym);
    yypush_ (m, t);
  }

  inline
  void
   Parser ::yypush_ (const char* m, stack_symbol_type& s)
  {
    if (m)
      YY_SYMBOL_PRINT (m, s);
    yystack_.push (s);
  }

  inline
  void
   Parser ::yypop_ (unsigned int n)
  {
    yystack_.pop (n);
  }

#if YYDEBUG
  std::ostream&
   Parser ::debug_stream () const
  {
    return *yycdebug_;
  }

  void
   Parser ::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


   Parser ::debug_level_type
   Parser ::debug_level () const
  {
    return yydebug_;
  }

  void
   Parser ::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif // YYDEBUG

  inline  Parser ::state_type
   Parser ::yy_lr_goto_state_ (state_type yystate, int yysym)
  {
    int yyr = yypgoto_[yysym - yyntokens_] + yystate;
    if (0 <= yyr && yyr <= yylast_ && yycheck_[yyr] == yystate)
      return yytable_[yyr];
    else
      return yydefgoto_[yysym - yyntokens_];
  }

  inline bool
   Parser ::yy_pact_value_is_default_ (int yyvalue)
  {
    return yyvalue == yypact_ninf_;
  }

  inline bool
   Parser ::yy_table_value_is_error_ (int yyvalue)
  {
    return yyvalue == yytable_ninf_;
  }

  int
   Parser ::parse ()
  {
    // State.
    int yyn;
    /// Length of the RHS of the rule being reduced.
    int yylen = 0;

    // Error handling.
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// The lookahead symbol.
    symbol_type yyla;

    /// The locations where the error started and ended.
    stack_symbol_type yyerror_range[3];

    /// The return value of parse ().
    int yyresult;

    // FIXME: This shoud be completely indented.  It is not yet to
    // avoid gratuitous conflicts when merging into the master branch.
    try
      {
    YYCDEBUG << "Starting parse" << std::endl;


    /* Initialize the stack.  The initial state will be set in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystack_.clear ();
    yypush_ (YY_NULLPTR, 0, yyla);

    // A new symbol was pushed on the stack.
  yynewstate:
    YYCDEBUG << "Entering state " << yystack_[0].state << std::endl;

    // Accept?
    if (yystack_[0].state == yyfinal_)
      goto yyacceptlab;

    goto yybackup;

    // Backup.
  yybackup:

    // Try to take a decision without lookahead.
    yyn = yypact_[yystack_[0].state];
    if (yy_pact_value_is_default_ (yyn))
      goto yydefault;

    // Read a lookahead token.
    if (yyla.empty ())
      {
        YYCDEBUG << "Reading a token: ";
        try
          {
            yyla.type = yytranslate_ (yylex (&yyla.value, &yyla.location));
          }
        catch (const syntax_error& yyexc)
          {
            error (yyexc);
            goto yyerrlab1;
          }
      }
    YY_SYMBOL_PRINT ("Next token is", yyla);

    /* If the proper action on seeing token YYLA.TYPE is to reduce or
       to detect an error, take that action.  */
    yyn += yyla.type_get ();
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yyla.type_get ())
      goto yydefault;

    // Reduce or error.
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
        if (yy_table_value_is_error_ (yyn))
          goto yyerrlab;
        yyn = -yyn;
        goto yyreduce;
      }

    // Count tokens shifted since error; after three, turn off error status.
    if (yyerrstatus_)
      --yyerrstatus_;

    // Shift the lookahead token.
    yypush_ ("Shifting", yyn, yyla);
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystack_[0].state];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    {
      stack_symbol_type yylhs;
      yylhs.state = yy_lr_goto_state_(yystack_[yylen].state, yyr1_[yyn]);
      /* Variants are always initialized to an empty instance of the
         correct type. The default '$$ = $1' action is NOT applied
         when using variants.  */
        switch (yyr1_[yyn])
    {
      case 66: // condition
        yylhs.value.build< bool > ();
        break;

      case 47: // REAL
        yylhs.value.build< float > ();
        break;

      case 46: // NUMBER
      case 70: // operation
      case 71: // nombre
        yylhs.value.build< int > ();
        break;

      case 48: // NAME
      case 49: // FICHIER
      case 50: // HEX_COLOR
      case 51: // N_TURTLE
      case 52: // C_N_TURTLE
        yylhs.value.build< std::string > ();
        break;

      default:
        break;
    }


      // Compute the default @$.
      {
        slice<stack_symbol_type, stack_type> slice (yystack_, yylen);
        YYLLOC_DEFAULT (yylhs.location, slice, yylen);
      }

      // Perform the reduction.
      YY_REDUCE_PRINT (yyn);
      try
        {
          switch (yyn)
            {
  case 2:
#line 60 "parser.yy" // lalr1.cc:859
    { }
#line 1117 "parser.tab.cc" // lalr1.cc:859
    break;

  case 4:
#line 65 "parser.yy" // lalr1.cc:859
    {}
#line 1123 "parser.tab.cc" // lalr1.cc:859
    break;

  case 5:
#line 66 "parser.yy" // lalr1.cc:859
    {}
#line 1129 "parser.tab.cc" // lalr1.cc:859
    break;

  case 6:
#line 67 "parser.yy" // lalr1.cc:859
    {}
#line 1135 "parser.tab.cc" // lalr1.cc:859
    break;

  case 7:
#line 71 "parser.yy" // lalr1.cc:859
    {
        	std::cout << "Function " << yystack_[3].value.as< std::string > () << "\n";	
	}
#line 1143 "parser.tab.cc" // lalr1.cc:859
    break;

  case 9:
#line 77 "parser.yy" // lalr1.cc:859
    {}
#line 1149 "parser.tab.cc" // lalr1.cc:859
    break;

  case 10:
#line 81 "parser.yy" // lalr1.cc:859
    {}
#line 1155 "parser.tab.cc" // lalr1.cc:859
    break;

  case 11:
#line 82 "parser.yy" // lalr1.cc:859
    {}
#line 1161 "parser.tab.cc" // lalr1.cc:859
    break;

  case 12:
#line 83 "parser.yy" // lalr1.cc:859
    {}
#line 1167 "parser.tab.cc" // lalr1.cc:859
    break;

  case 13:
#line 84 "parser.yy" // lalr1.cc:859
    {}
#line 1173 "parser.tab.cc" // lalr1.cc:859
    break;

  case 14:
#line 85 "parser.yy" // lalr1.cc:859
    {}
#line 1179 "parser.tab.cc" // lalr1.cc:859
    break;

  case 15:
#line 86 "parser.yy" // lalr1.cc:859
    {}
#line 1185 "parser.tab.cc" // lalr1.cc:859
    break;

  case 16:
#line 87 "parser.yy" // lalr1.cc:859
    {}
#line 1191 "parser.tab.cc" // lalr1.cc:859
    break;

  case 17:
#line 88 "parser.yy" // lalr1.cc:859
    {}
#line 1197 "parser.tab.cc" // lalr1.cc:859
    break;

  case 18:
#line 89 "parser.yy" // lalr1.cc:859
    {
        	std::cout << "TORTUES: " << yystack_[0].value.as< int > () << "\n";
			driver.addTurtles(yystack_[0].value.as< int > ());
	}
#line 1206 "parser.tab.cc" // lalr1.cc:859
    break;

  case 19:
#line 93 "parser.yy" // lalr1.cc:859
    {
        	std::cout << "JARDIN: " << yystack_[0].value.as< std::string > () << "\n";
//			driver.loadJardin($2);
	}
#line 1215 "parser.tab.cc" // lalr1.cc:859
    break;

  case 20:
#line 100 "parser.yy" // lalr1.cc:859
    {
		std::cout << "REPETE: " << yystack_[4].value.as< int > () << " fois\n";
	}
#line 1223 "parser.tab.cc" // lalr1.cc:859
    break;

  case 21:
#line 106 "parser.yy" // lalr1.cc:859
    {}
#line 1229 "parser.tab.cc" // lalr1.cc:859
    break;

  case 22:
#line 107 "parser.yy" // lalr1.cc:859
    {}
#line 1235 "parser.tab.cc" // lalr1.cc:859
    break;

  case 23:
#line 111 "parser.yy" // lalr1.cc:859
    {}
#line 1241 "parser.tab.cc" // lalr1.cc:859
    break;

  case 24:
#line 112 "parser.yy" // lalr1.cc:859
    {}
#line 1247 "parser.tab.cc" // lalr1.cc:859
    break;

  case 25:
#line 113 "parser.yy" // lalr1.cc:859
    {}
#line 1253 "parser.tab.cc" // lalr1.cc:859
    break;

  case 26:
#line 114 "parser.yy" // lalr1.cc:859
    {}
#line 1259 "parser.tab.cc" // lalr1.cc:859
    break;

  case 27:
#line 118 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 MUR GAUCHE\n";
		yylhs.value.as< bool > () = driver.estMur(0, direction::west);
	}
#line 1268 "parser.tab.cc" // lalr1.cc:859
    break;

  case 28:
#line 122 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 MUR DROITE\n";
		yylhs.value.as< bool > () = driver.estMur(0, direction::east);
	}
#line 1277 "parser.tab.cc" // lalr1.cc:859
    break;

  case 29:
#line 126 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 MUR DEVANT\n";
		yylhs.value.as< bool > () = driver.estMur(0, direction::north);
	}
#line 1286 "parser.tab.cc" // lalr1.cc:859
    break;

  case 30:
#line 130 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 MUR DERRIERE\n";
		yylhs.value.as< bool > () = driver.estMur(0, direction::south);
	}
#line 1295 "parser.tab.cc" // lalr1.cc:859
    break;

  case 31:
#line 134 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 VIDE GAUCHE\n";
		yylhs.value.as< bool > () = driver.estVide(0, direction::west);
	}
#line 1304 "parser.tab.cc" // lalr1.cc:859
    break;

  case 32:
#line 138 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 VIDE DROITE\n";
		yylhs.value.as< bool > () = driver.estVide(0, direction::east);
	}
#line 1313 "parser.tab.cc" // lalr1.cc:859
    break;

  case 33:
#line 142 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 VIDE DEVANT\n";
		yylhs.value.as< bool > () = driver.estVide(0, direction::north);
	}
#line 1322 "parser.tab.cc" // lalr1.cc:859
    break;

  case 34:
#line 146 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 VIDE DERRIERE\n";
		yylhs.value.as< bool > () = driver.estVide(0, direction::south);
	}
#line 1331 "parser.tab.cc" // lalr1.cc:859
    break;

  case 35:
#line 150 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " MUR GAUCHE\n";
		yylhs.value.as< bool > () = driver.estMur(driver.getTurtle(yystack_[0].value.as< std::string > ()), direction::west);
	}
#line 1340 "parser.tab.cc" // lalr1.cc:859
    break;

  case 36:
#line 154 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " MUR DROITE\n";
		yylhs.value.as< bool > () = driver.estMur(driver.getTurtle(yystack_[0].value.as< std::string > ()), direction::east);
	}
#line 1349 "parser.tab.cc" // lalr1.cc:859
    break;

  case 37:
#line 158 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " MUR DEVANT\n";
		yylhs.value.as< bool > () = driver.estMur(driver.getTurtle(yystack_[0].value.as< std::string > ()), direction::north);
	}
#line 1358 "parser.tab.cc" // lalr1.cc:859
    break;

  case 38:
#line 162 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " MUR DERRIERE\n";
		yylhs.value.as< bool > () = driver.estMur(driver.getTurtle(yystack_[0].value.as< std::string > ()), direction::south);
	}
#line 1367 "parser.tab.cc" // lalr1.cc:859
    break;

  case 39:
#line 166 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " VIDE GAUCHE\n";
		yylhs.value.as< bool > () = driver.estVide(driver.getTurtle(yystack_[0].value.as< std::string > ()), direction::west);
	}
#line 1376 "parser.tab.cc" // lalr1.cc:859
    break;

  case 40:
#line 170 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " VIDE DROITE\n";
		yylhs.value.as< bool > () = driver.estVide(driver.getTurtle(yystack_[0].value.as< std::string > ()), direction::east);
	}
#line 1385 "parser.tab.cc" // lalr1.cc:859
    break;

  case 41:
#line 174 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " VIDE DEVANT\n";
		yylhs.value.as< bool > () = driver.estVide(driver.getTurtle(yystack_[0].value.as< std::string > ()), direction::north);
	}
#line 1394 "parser.tab.cc" // lalr1.cc:859
    break;

  case 42:
#line 178 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " VIDE DERRIERE\n";
		yylhs.value.as< bool > () = driver.estVide(driver.getTurtle(yystack_[0].value.as< std::string > ()), direction::south);
	}
#line 1403 "parser.tab.cc" // lalr1.cc:859
    break;

  case 43:
#line 185 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 Avance: 1" << "\n";
		driver.nAvanceN(0, 1);
	}
#line 1412 "parser.tab.cc" // lalr1.cc:859
    break;

  case 44:
#line 189 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 Avance: " << yystack_[0].value.as< int > () << "\n";
		driver.nAvanceN(0, yystack_[0].value.as< int > ());
	}
#line 1421 "parser.tab.cc" // lalr1.cc:859
    break;

  case 45:
#line 193 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 Avance: " << yystack_[1].value.as< int > () << " fois\n";
		driver.nAvanceN(0, yystack_[1].value.as< int > ());
	}
#line 1430 "parser.tab.cc" // lalr1.cc:859
    break;

  case 46:
#line 197 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 Recule: 1\n";
		driver.nReculeN(0, 1);
	}
#line 1439 "parser.tab.cc" // lalr1.cc:859
    break;

  case 47:
#line 201 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 Recule: " << yystack_[0].value.as< int > () << "\n";
		driver.nReculeN(0, yystack_[0].value.as< int > ());
	}
#line 1448 "parser.tab.cc" // lalr1.cc:859
    break;

  case 48:
#line 205 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 Recule: " << yystack_[1].value.as< int > () << " fois\n";
		driver.nReculeN(0, yystack_[1].value.as< int > ());
	}
#line 1457 "parser.tab.cc" // lalr1.cc:859
    break;

  case 49:
#line 209 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 Saute:  1\n";
		driver.nSauteN(0, 1);
	}
#line 1466 "parser.tab.cc" // lalr1.cc:859
    break;

  case 50:
#line 213 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 Saute:  " << yystack_[0].value.as< int > () << "\n";
		driver.nSauteN(0, yystack_[0].value.as< int > ());
	}
#line 1475 "parser.tab.cc" // lalr1.cc:859
    break;

  case 51:
#line 217 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 Saute:  " << yystack_[1].value.as< int > () << " fois\n";
		driver.nSauteN(0, yystack_[1].value.as< int > ());
	}
#line 1484 "parser.tab.cc" // lalr1.cc:859
    break;

  case 52:
#line 221 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " Avance: 1" << "\n";
		driver.nAvanceN(driver.getTurtle(yystack_[0].value.as< std::string > ()), 1);
	}
#line 1493 "parser.tab.cc" // lalr1.cc:859
    break;

  case 53:
#line 225 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " Avance: " << yystack_[1].value.as< int > () << "\n";
		driver.nAvanceN(driver.getTurtle(yystack_[0].value.as< std::string > ()), yystack_[1].value.as< int > ());
	}
#line 1502 "parser.tab.cc" // lalr1.cc:859
    break;

  case 54:
#line 229 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " Avance: " << yystack_[2].value.as< int > () << " fois\n";
		driver.nAvanceN(driver.getTurtle(yystack_[0].value.as< std::string > ()), yystack_[2].value.as< int > ());
	}
#line 1511 "parser.tab.cc" // lalr1.cc:859
    break;

  case 55:
#line 233 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " Recule: 1\n";
		driver.nReculeN(driver.getTurtle(yystack_[0].value.as< std::string > ()), 1);
	}
#line 1520 "parser.tab.cc" // lalr1.cc:859
    break;

  case 56:
#line 237 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " Recule: " << yystack_[1].value.as< int > () << "\n";
		driver.nReculeN(driver.getTurtle(yystack_[0].value.as< std::string > ()), yystack_[1].value.as< int > ());
	}
#line 1529 "parser.tab.cc" // lalr1.cc:859
    break;

  case 57:
#line 241 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " Recule: " << yystack_[2].value.as< int > () << " fois\n";
		driver.nReculeN(driver.getTurtle(yystack_[0].value.as< std::string > ()), yystack_[2].value.as< int > ());
	}
#line 1538 "parser.tab.cc" // lalr1.cc:859
    break;

  case 58:
#line 245 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " Saute:  1\n";
		driver.nSauteN(driver.getTurtle(yystack_[0].value.as< std::string > ()), 1);
	}
#line 1547 "parser.tab.cc" // lalr1.cc:859
    break;

  case 59:
#line 249 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " Saute:  " << yystack_[1].value.as< int > () << "\n";
		driver.nSauteN(driver.getTurtle(yystack_[0].value.as< std::string > ()), yystack_[1].value.as< int > ());
	}
#line 1556 "parser.tab.cc" // lalr1.cc:859
    break;

  case 60:
#line 253 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " Saute:  " << yystack_[2].value.as< int > () << " fois\n";
		driver.nSauteN(driver.getTurtle(yystack_[0].value.as< std::string > ()), yystack_[2].value.as< int > ());
	}
#line 1565 "parser.tab.cc" // lalr1.cc:859
    break;

  case 61:
#line 260 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 Tourne:  Droite\n";
		driver.nTourneN(0, 'd', 1);
	}
#line 1574 "parser.tab.cc" // lalr1.cc:859
    break;

  case 62:
#line 264 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 Tourne:  Gauche\n";
		driver.nTourneN(0, 'g', 1);
	}
#line 1583 "parser.tab.cc" // lalr1.cc:859
    break;

  case 63:
#line 268 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 Tourne:  Droite (x" << yystack_[1].value.as< int > () << ")\n";
		driver.nTourneN(0, 'd', yystack_[1].value.as< int > ());
	}
#line 1592 "parser.tab.cc" // lalr1.cc:859
    break;

  case 64:
#line 272 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 Tourne:  Gauche (x" << yystack_[1].value.as< int > () << ")\n";
		driver.nTourneN(0, 'g', yystack_[1].value.as< int > ());
	}
#line 1601 "parser.tab.cc" // lalr1.cc:859
    break;

  case 65:
#line 276 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " Tourne:  Droite\n";
		driver.nTourneN(driver.getTurtle(yystack_[0].value.as< std::string > ()), 'd', 1);
	}
#line 1610 "parser.tab.cc" // lalr1.cc:859
    break;

  case 66:
#line 280 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " Tourne:  Gauche\n";
		driver.nTourneN(driver.getTurtle(yystack_[0].value.as< std::string > ()), 'g', 1);
	}
#line 1619 "parser.tab.cc" // lalr1.cc:859
    break;

  case 67:
#line 284 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " Tourne:  Droite (x" << yystack_[2].value.as< int > () << ")\n";
		driver.nTourneN(driver.getTurtle(yystack_[0].value.as< std::string > ()), 'd', yystack_[2].value.as< int > ());
	}
#line 1628 "parser.tab.cc" // lalr1.cc:859
    break;

  case 68:
#line 288 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " Tourne:  Gauche (x" << yystack_[2].value.as< int > () << ")\n";
		driver.nTourneN(driver.getTurtle(yystack_[0].value.as< std::string > ()), 'g', yystack_[2].value.as< int > ());
	}
#line 1637 "parser.tab.cc" // lalr1.cc:859
    break;

  case 69:
#line 295 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 CARAPACE: " << yystack_[0].value.as< std::string > () << "\n";
		driver.nSetCarapace(0, yystack_[0].value.as< std::string > ());
	}
#line 1646 "parser.tab.cc" // lalr1.cc:859
    break;

  case 70:
#line 299 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 CARAPACE: " << yystack_[0].value.as< std::string > () << "\n";
		driver.nSetCarapace(0, yystack_[0].value.as< std::string > ());
	}
#line 1655 "parser.tab.cc" // lalr1.cc:859
    break;

  case 71:
#line 303 "parser.yy" // lalr1.cc:859
    {
		std::cout << "@1 MOTIF   : " << yystack_[0].value.as< std::string > () << "\n";
		driver.nSetMotif(0, yystack_[0].value.as< std::string > ());
	}
#line 1664 "parser.tab.cc" // lalr1.cc:859
    break;

  case 72:
#line 307 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " CARAPACE: " << yystack_[1].value.as< std::string > () << "\n";
		driver.nSetCarapace(driver.getTurtle(yystack_[0].value.as< std::string > ()), yystack_[1].value.as< std::string > ());
	}
#line 1673 "parser.tab.cc" // lalr1.cc:859
    break;

  case 73:
#line 311 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " CARAPACE: " << yystack_[1].value.as< std::string > () << "\n";
		driver.nSetCarapace(driver.getTurtle(yystack_[0].value.as< std::string > ()), yystack_[1].value.as< std::string > ());
	}
#line 1682 "parser.tab.cc" // lalr1.cc:859
    break;

  case 74:
#line 315 "parser.yy" // lalr1.cc:859
    {
		std::cout << yystack_[0].value.as< std::string > () << " MOTIF   : " << yystack_[1].value.as< std::string > () << "\n";
		driver.nSetMotif(driver.getTurtle(yystack_[0].value.as< std::string > ()), yystack_[1].value.as< std::string > ());
	}
#line 1691 "parser.tab.cc" // lalr1.cc:859
    break;

  case 75:
#line 322 "parser.yy" // lalr1.cc:859
    {			yylhs.value.as< int > () = yystack_[0].value.as< int > ();	}
#line 1697 "parser.tab.cc" // lalr1.cc:859
    break;

  case 76:
#line 323 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = yystack_[2].value.as< int > () + yystack_[0].value.as< int > ();	}
#line 1703 "parser.tab.cc" // lalr1.cc:859
    break;

  case 77:
#line 324 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = yystack_[2].value.as< int > () - yystack_[0].value.as< int > ();	}
#line 1709 "parser.tab.cc" // lalr1.cc:859
    break;

  case 78:
#line 325 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = yystack_[2].value.as< int > () * yystack_[0].value.as< int > ();	}
#line 1715 "parser.tab.cc" // lalr1.cc:859
    break;

  case 79:
#line 326 "parser.yy" // lalr1.cc:859
    {
		if (yystack_[0].value.as< int > () == 0) {
		  	yy::Parser::error(yystack_[0].location, "[ERROR] Division by zero\n");
		} else {		yylhs.value.as< int > () = yystack_[2].value.as< int > () / yystack_[0].value.as< int > ();	}
	}
#line 1725 "parser.tab.cc" // lalr1.cc:859
    break;

  case 80:
#line 331 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = -yystack_[0].value.as< int > ();	}
#line 1731 "parser.tab.cc" // lalr1.cc:859
    break;

  case 81:
#line 332 "parser.yy" // lalr1.cc:859
    {   	yylhs.value.as< int > () = -yystack_[1].value.as< int > (); 	}
#line 1737 "parser.tab.cc" // lalr1.cc:859
    break;

  case 82:
#line 333 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = yystack_[1].value.as< int > ();  	}
#line 1743 "parser.tab.cc" // lalr1.cc:859
    break;

  case 83:
#line 334 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = yystack_[2].value.as< int > () < yystack_[0].value.as< int > ();  	}
#line 1749 "parser.tab.cc" // lalr1.cc:859
    break;

  case 84:
#line 335 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = yystack_[2].value.as< int > () > yystack_[0].value.as< int > ();  	}
#line 1755 "parser.tab.cc" // lalr1.cc:859
    break;

  case 85:
#line 336 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = yystack_[2].value.as< int > () <= yystack_[0].value.as< int > (); 	}
#line 1761 "parser.tab.cc" // lalr1.cc:859
    break;

  case 86:
#line 337 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = yystack_[2].value.as< int > () >= yystack_[0].value.as< int > (); 	}
#line 1767 "parser.tab.cc" // lalr1.cc:859
    break;

  case 87:
#line 338 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = yystack_[2].value.as< int > () == yystack_[0].value.as< int > (); 	}
#line 1773 "parser.tab.cc" // lalr1.cc:859
    break;

  case 88:
#line 339 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = yystack_[2].value.as< int > () != yystack_[0].value.as< int > (); 	}
#line 1779 "parser.tab.cc" // lalr1.cc:859
    break;

  case 89:
#line 340 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = yystack_[2].value.as< int > () && yystack_[0].value.as< int > ();	}
#line 1785 "parser.tab.cc" // lalr1.cc:859
    break;

  case 90:
#line 341 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = yystack_[2].value.as< int > () || yystack_[0].value.as< int > ();	}
#line 1791 "parser.tab.cc" // lalr1.cc:859
    break;

  case 91:
#line 342 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = (yystack_[2].value.as< int > () || yystack_[0].value.as< int > ()) && !(yystack_[2].value.as< int > () && yystack_[0].value.as< int > ());}
#line 1797 "parser.tab.cc" // lalr1.cc:859
    break;

  case 92:
#line 343 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = !yystack_[0].value.as< int > ();	}
#line 1803 "parser.tab.cc" // lalr1.cc:859
    break;

  case 93:
#line 349 "parser.yy" // lalr1.cc:859
    {	yylhs.value.as< int > () = yystack_[0].value.as< int > ();    	}
#line 1809 "parser.tab.cc" // lalr1.cc:859
    break;


#line 1813 "parser.tab.cc" // lalr1.cc:859
            default:
              break;
            }
        }
      catch (const syntax_error& yyexc)
        {
          error (yyexc);
          YYERROR;
        }
      YY_SYMBOL_PRINT ("-> $$ =", yylhs);
      yypop_ (yylen);
      yylen = 0;
      YY_STACK_PRINT ();

      // Shift the result of the reduction.
      yypush_ (YY_NULLPTR, yylhs);
    }
    goto yynewstate;

  /*--------------------------------------.
  | yyerrlab -- here on detecting error.  |
  `--------------------------------------*/
  yyerrlab:
    // If not already recovering from an error, report this error.
    if (!yyerrstatus_)
      {
        ++yynerrs_;
        error (yyla.location, yysyntax_error_ (yystack_[0].state, yyla));
      }


    yyerror_range[1].location = yyla.location;
    if (yyerrstatus_ == 3)
      {
        /* If just tried and failed to reuse lookahead token after an
           error, discard it.  */

        // Return failure if at end of input.
        if (yyla.type_get () == yyeof_)
          YYABORT;
        else if (!yyla.empty ())
          {
            yy_destroy_ ("Error: discarding", yyla);
            yyla.clear ();
          }
      }

    // Else will try to reuse lookahead token after shifting the error token.
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;
    yyerror_range[1].location = yystack_[yylen - 1].location;
    /* Do not reclaim the symbols of the rule whose action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;   // Each real token shifted decrements this.
    {
      stack_symbol_type error_token;
      for (;;)
        {
          yyn = yypact_[yystack_[0].state];
          if (!yy_pact_value_is_default_ (yyn))
            {
              yyn += yyterror_;
              if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
                {
                  yyn = yytable_[yyn];
                  if (0 < yyn)
                    break;
                }
            }

          // Pop the current state because it cannot handle the error token.
          if (yystack_.size () == 1)
            YYABORT;

          yyerror_range[1].location = yystack_[0].location;
          yy_destroy_ ("Error: popping", yystack_[0]);
          yypop_ ();
          YY_STACK_PRINT ();
        }

      yyerror_range[2].location = yyla.location;
      YYLLOC_DEFAULT (error_token.location, yyerror_range, 2);

      // Shift the error token.
      error_token.state = yyn;
      yypush_ ("Shifting", error_token);
    }
    goto yynewstate;

    // Accept.
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    // Abort.
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (!yyla.empty ())
      yy_destroy_ ("Cleanup: discarding lookahead", yyla);

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (1 < yystack_.size ())
      {
        yy_destroy_ ("Cleanup: popping", yystack_[0]);
        yypop_ ();
      }

    return yyresult;
  }
    catch (...)
      {
        YYCDEBUG << "Exception caught: cleaning lookahead and stack"
                 << std::endl;
        // Do not try to display the values of the reclaimed symbols,
        // as their printer might throw an exception.
        if (!yyla.empty ())
          yy_destroy_ (YY_NULLPTR, yyla);

        while (1 < yystack_.size ())
          {
            yy_destroy_ (YY_NULLPTR, yystack_[0]);
            yypop_ ();
          }
        throw;
      }
  }

  void
   Parser ::error (const syntax_error& yyexc)
  {
    error (yyexc.location, yyexc.what());
  }

  // Generate an error message.
  std::string
   Parser ::yysyntax_error_ (state_type, const symbol_type&) const
  {
    return YY_("syntax error");
  }


  const signed char  Parser ::yypact_ninf_ = -38;

  const signed char  Parser ::yytable_ninf_ = -1;

  const short int
   Parser ::yypact_[] =
  {
     -38,   147,   -38,   -38,     4,   -21,   -38,   -38,   -38,   -38,
     221,   -38,   -38,    21,   -37,    44,   -19,     8,     9,    10,
     -25,    51,    44,   -18,   -38,   -38,   -38,   -38,   -38,   -38,
     -38,   -38,    11,   323,   337,   -38,   125,    44,    44,   -38,
     148,   -38,    11,   -38,   -38,    28,   -38,    80,   -38,   107,
      97,   124,     2,    15,    30,   420,   -38,   -38,    29,    36,
     -38,   -38,   -38,   -38,    37,    48,    52,    56,   -38,   -38,
     -38,   -38,    57,    64,   166,    44,   434,   388,   123,    44,
      44,    44,    44,    44,    44,    44,    44,    44,    44,    44,
      44,    44,   -38,   -38,   242,    31,   -38,    76,   -38,    77,
     -38,   -38,   197,   -38,   222,   -38,    89,    90,   256,   -38,
     -38,   -38,   -38,   -38,   -38,   -38,   -38,   126,   -38,   404,
     -38,   434,   434,   165,   165,   -38,   -38,   -38,   -38,   -38,
     -38,   123,   123,   123,   275,   289,   120,   -38,   -38,   -38,
     100,   105,   -38,   -38,   -37,   -38,   -38,   310,   -38,    78,
     131,   -38,   -38,   -38,   324,   149,   -38,   -38,   150,   -38,
     -38
  };

  const unsigned char
   Parser ::yydefact_[] =
  {
       2,     0,     1,     4,     0,     0,     3,     6,     5,     8,
       0,    10,    11,     0,     0,     0,     0,    43,    46,    49,
       0,     0,     0,     0,     9,    14,    13,    12,    15,    16,
      17,     7,     0,     0,     0,     8,     0,     0,     0,    93,
       0,    75,     0,     8,    52,    44,    55,    47,    58,    50,
      62,    61,    69,     0,     0,    18,    19,     8,     0,     0,
      27,    28,    29,    30,     0,     0,     0,     0,    31,    32,
      33,    34,     0,     0,     0,     0,    80,     0,    92,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     8,     8,     0,    45,    53,    48,    56,    51,
      59,    66,     0,    65,     0,    72,    70,    71,     0,    35,
      36,    37,    38,    39,    40,    41,    42,     0,     8,     0,
      82,    76,    77,    78,    79,    83,    84,    85,    86,    87,
      88,    89,    90,    91,     0,     0,     0,    54,    57,    60,
      64,    63,    73,    74,    25,     8,    23,     0,    81,     0,
       0,    21,    68,    67,     0,     0,    20,    22,     0,    24,
      26
  };

  const signed char
   Parser ::yypgoto_[] =
  {
     -38,   -38,   -38,   -38,   -35,   -38,   -38,   -38,   -38,    -6,
     -38,   -38,   -38,   -13,   -38
  };

  const signed char
   Parser ::yydefgoto_[] =
  {
      -1,     1,     6,     7,    10,    24,    25,    26,    27,    35,
      28,    29,    30,    40,    41
  };

  const unsigned char
   Parser ::yytable_[] =
  {
      74,    32,    33,    34,    45,    47,    49,     8,    94,    55,
      43,    50,    51,    36,    36,    36,    37,    37,    37,    42,
      33,    34,   108,    76,    77,    78,    57,     9,    38,    38,
      38,    56,    79,    80,    81,    82,    93,   102,   104,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    31,    36,
      33,    34,    37,   105,    39,    39,    39,   134,   135,    44,
      46,    48,   119,    95,    38,   106,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,    96,
     107,   109,   137,   147,    79,    80,    81,    82,   110,   111,
      39,    83,    84,    85,    86,    87,    88,    89,    90,    91,
     112,    52,    36,   156,   113,    37,    53,    54,   114,   115,
     154,    79,    80,    81,    82,    97,   116,    38,    83,    84,
      85,    86,    87,    88,    89,    90,    91,   138,   139,    36,
      36,    98,    37,    75,    83,    84,    85,    86,    87,    88,
     142,   143,    99,    39,    38,    38,   151,     2,   101,   146,
       3,   152,    79,    80,    81,    82,   153,   157,   100,    83,
      84,    85,    86,    87,    88,    89,    90,    91,     4,    11,
      39,    39,   159,   160,     5,   103,    83,    84,    85,    86,
      87,    88,    89,    90,    91,     0,     0,    12,   117,    14,
     118,    15,    16,    92,    17,    18,    19,    20,    21,    22,
      23,    79,    80,    81,    82,     0,     0,     0,    83,    84,
      85,    86,    87,    88,    89,    90,    91,     0,     0,     0,
       0,     0,     0,     0,    11,     0,    79,    80,    81,    82,
       0,     0,   140,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    12,    13,    14,    11,    15,    16,     0,    17,
      18,    19,    20,    21,    22,    23,     0,   141,     0,    11,
       0,     0,     0,    12,   136,    14,     0,    15,    16,     0,
      17,    18,    19,    20,    21,    22,    23,    12,    11,   144,
     145,    15,    16,     0,    17,    18,    19,    20,    21,    22,
      23,     0,    11,     0,     0,     0,    12,   149,    14,     0,
      15,    16,     0,    17,    18,    19,    20,    21,    22,    23,
      12,   150,    14,    11,    15,    16,     0,    17,    18,    19,
      20,    21,    22,    23,     0,     0,     0,    11,     0,     0,
       0,    12,   155,    14,     0,    15,    16,     0,    17,    18,
      19,    20,    21,    22,    23,    12,   158,    14,     0,    15,
      16,     0,    17,    18,    19,    20,    21,    22,    23,    58,
      59,     0,     0,     0,    60,    61,    62,    63,     0,     0,
       0,     0,     0,    66,    67,     0,    64,    65,    68,    69,
      70,    71,     0,     0,     0,     0,     0,     0,     0,     0,
      72,    73,    79,    80,    81,    82,     0,   120,     0,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    79,    80,
      81,    82,     0,   148,     0,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    79,    80,    81,    82,     0,     0,
       0,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      81,    82,     0,     0,     0,    83,    84,    85,    86,    87,
      88,    89,    90,    91
  };

  const short int
   Parser ::yycheck_[] =
  {
      35,    38,    39,    40,    17,    18,    19,     3,    43,    22,
      16,    36,    37,     5,     5,     5,     8,     8,     8,    38,
      39,    40,    57,    36,    37,    38,    32,    48,    20,    20,
      20,    49,     4,     5,     6,     7,    42,    50,    51,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    27,     5,
      39,    40,     8,    51,    46,    46,    46,    92,    93,    51,
      51,    51,    75,    35,    20,    50,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    51,
      50,    52,    51,   118,     4,     5,     6,     7,    52,    52,
      46,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      52,    50,     5,    25,    52,     8,    55,    56,    52,    52,
     145,     4,     5,     6,     7,    35,    52,    20,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    51,    51,     5,
       5,    51,     8,     8,    11,    12,    13,    14,    15,    16,
      51,    51,    35,    46,    20,    20,    26,     0,    51,    23,
       3,    51,     4,     5,     6,     7,    51,    26,    51,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    21,     3,
      46,    46,    23,    23,    27,    51,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    -1,    -1,    21,    22,    23,
      24,    25,    26,    45,    28,    29,    30,    31,    32,    33,
      34,     4,     5,     6,     7,    -1,    -1,    -1,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     3,    -1,     4,     5,     6,     7,
      -1,    -1,    35,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    21,    22,    23,     3,    25,    26,    -1,    28,
      29,    30,    31,    32,    33,    34,    -1,    35,    -1,     3,
      -1,    -1,    -1,    21,    22,    23,    -1,    25,    26,    -1,
      28,    29,    30,    31,    32,    33,    34,    21,     3,    23,
      24,    25,    26,    -1,    28,    29,    30,    31,    32,    33,
      34,    -1,     3,    -1,    -1,    -1,    21,    22,    23,    -1,
      25,    26,    -1,    28,    29,    30,    31,    32,    33,    34,
      21,    22,    23,     3,    25,    26,    -1,    28,    29,    30,
      31,    32,    33,    34,    -1,    -1,    -1,     3,    -1,    -1,
      -1,    21,    22,    23,    -1,    25,    26,    -1,    28,    29,
      30,    31,    32,    33,    34,    21,    22,    23,    -1,    25,
      26,    -1,    28,    29,    30,    31,    32,    33,    34,    36,
      37,    -1,    -1,    -1,    41,    42,    43,    44,    -1,    -1,
      -1,    -1,    -1,    36,    37,    -1,    53,    54,    41,    42,
      43,    44,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      53,    54,     4,     5,     6,     7,    -1,     9,    -1,    11,
      12,    13,    14,    15,    16,    17,    18,    19,     4,     5,
       6,     7,    -1,     9,    -1,    11,    12,    13,    14,    15,
      16,    17,    18,    19,     4,     5,     6,     7,    -1,    -1,
      -1,    11,    12,    13,    14,    15,    16,    17,    18,    19,
       6,     7,    -1,    -1,    -1,    11,    12,    13,    14,    15,
      16,    17,    18,    19
  };

  const unsigned char
   Parser ::yystos_[] =
  {
       0,    58,     0,     3,    21,    27,    59,    60,     3,    48,
      61,     3,    21,    22,    23,    25,    26,    28,    29,    30,
      31,    32,    33,    34,    62,    63,    64,    65,    67,    68,
      69,    27,    38,    39,    40,    66,     5,     8,    20,    46,
      70,    71,    38,    66,    51,    70,    51,    70,    51,    70,
      36,    37,    50,    55,    56,    70,    49,    66,    36,    37,
      41,    42,    43,    44,    53,    54,    36,    37,    41,    42,
      43,    44,    53,    54,    61,     8,    70,    70,    70,     4,
       5,     6,     7,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    45,    66,    61,    35,    51,    35,    51,    35,
      51,    51,    70,    51,    70,    51,    50,    50,    61,    52,
      52,    52,    52,    52,    52,    52,    52,    22,    24,    70,
       9,    70,    70,    70,    70,    70,    70,    70,    70,    70,
      70,    70,    70,    70,    61,    61,    22,    51,    51,    51,
      35,    35,    51,    51,    23,    24,    23,    61,     9,    22,
      22,    26,    51,    51,    61,    22,    25,    26,    22,    23,
      23
  };

  const unsigned char
   Parser ::yyr1_[] =
  {
       0,    57,    58,    58,    59,    59,    59,    60,    61,    61,
      62,    62,    62,    62,    62,    62,    62,    62,    62,    62,
      63,    64,    64,    65,    65,    65,    65,    66,    66,    66,
      66,    66,    66,    66,    66,    66,    66,    66,    66,    66,
      66,    66,    66,    67,    67,    67,    67,    67,    67,    67,
      67,    67,    67,    67,    67,    67,    67,    67,    67,    67,
      67,    68,    68,    68,    68,    68,    68,    68,    68,    69,
      69,    69,    69,    69,    69,    70,    70,    70,    70,    70,
      70,    70,    70,    70,    70,    70,    70,    70,    70,    70,
      70,    70,    70,    71
  };

  const unsigned char
   Parser ::yyr2_[] =
  {
       0,     2,     0,     2,     1,     2,     1,     5,     0,     2,
       1,     1,     1,     1,     1,     1,     1,     1,     2,     2,
       6,     5,     6,     5,     7,     5,     8,     2,     2,     2,
       2,     2,     2,     2,     2,     3,     3,     3,     3,     3,
       3,     3,     3,     1,     2,     3,     1,     2,     3,     1,
       2,     3,     2,     3,     4,     2,     3,     4,     2,     3,
       4,     2,     2,     4,     4,     3,     3,     5,     5,     2,
       3,     3,     3,     4,     4,     1,     3,     3,     3,     3,
       2,     4,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     2,     1
  };


#if YYDEBUG
  // YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
  // First, the terminals, then, starting at \a yyntokens_, nonterminals.
  const char*
  const  Parser ::yytname_[] =
  {
  "$end", "error", "$undefined", "NL", "ADD", "NEG", "MUL", "DIV", "PO",
  "PC", "EQU", "LT", "GT", "LET", "GET", "EQ", "NEQ", "AND", "OR", "XOR",
  "NOT", "COMMENTAIRE", "FIN", "SI", "SINON", "REPETE", "TANT_QUE",
  "FONCTION", "AVANCE", "RECULE", "SAUTE", "TOURNE", "COULEUR", "TORTUES",
  "JARDIN", "FOIS", "GAUCHE", "DROITE", "PAS", "MUR", "VIDE", "C_GAUCHE",
  "C_DROITE", "C_DEVANT", "C_DERRIERE", "C_FOIS", "NUMBER", "REAL", "NAME",
  "FICHIER", "HEX_COLOR", "N_TURTLE", "C_N_TURTLE", "\"devant\"",
  "\"derriere\"", "\"carapace\"", "\"motif\"", "$accept", "Input", "Ligne",
  "Fonction", "Instructions", "Instruction", "Rp_stmt", "Tq_stmt",
  "If_stmt", "condition", "deplacement", "direction", "couleur",
  "operation", "nombre", YY_NULLPTR
  };


  const unsigned short int
   Parser ::yyrline_[] =
  {
       0,    60,    60,    61,    65,    66,    67,    71,    76,    77,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    93,
     100,   106,   107,   111,   112,   113,   114,   118,   122,   126,
     130,   134,   138,   142,   146,   150,   154,   158,   162,   166,
     170,   174,   178,   185,   189,   193,   197,   201,   205,   209,
     213,   217,   221,   225,   229,   233,   237,   241,   245,   249,
     253,   260,   264,   268,   272,   276,   280,   284,   288,   295,
     299,   303,   307,   311,   315,   322,   323,   324,   325,   326,
     331,   332,   333,   334,   335,   336,   337,   338,   339,   340,
     341,   342,   343,   349
  };

  // Print the state stack on the debug stream.
  void
   Parser ::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (stack_type::const_iterator
           i = yystack_.begin (),
           i_end = yystack_.end ();
         i != i_end; ++i)
      *yycdebug_ << ' ' << i->state;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
   Parser ::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    // Print the symbols being reduced, and their result.
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
               << " (line " << yylno << "):" << std::endl;
    // The symbols being reduced.
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
                       yystack_[(yynrhs) - (yyi + 1)]);
  }
#endif // YYDEBUG

  // Symbol number corresponding to token number t.
  inline
   Parser ::token_number_type
   Parser ::yytranslate_ (int t)
  {
    static
    const token_number_type
    translate_table[] =
    {
     0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56
    };
    const unsigned int user_token_number_max_ = 311;
    const token_number_type undef_token_ = 2;

    if (static_cast<int>(t) <= yyeof_)
      return yyeof_;
    else if (static_cast<unsigned int> (t) <= user_token_number_max_)
      return translate_table[t];
    else
      return undef_token_;
  }


} // yy
#line 2315 "parser.tab.cc" // lalr1.cc:1167
#line 352 "parser.yy" // lalr1.cc:1168


void yy::Parser::error( const location_type &l, const std::string & err_msg) {
    std::cerr << "Erreur : " << l << ", " << err_msg << std::endl;
    exit(1);
}
