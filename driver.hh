//
// driver.hh for  in /home/venatum/rendu/L3-Angers/Compil/Projet
//
// Made by Venatum
// Login   <vincentlequec@gmail.com>
//
// Started on  Wed Nov 28 13:04:27 2018 Venatum
// Last update Thu Dec 13 18:47:24 2018 Venatum
//

#ifndef DRIVER_H
#define DRIVER_H

#include <map>
#include <vector>
#include <string>
#include <QPoint>
#include <QColor>
#include <QSize>
#include <ostream>

enum direction {
	north	= 0,
	east	= 90,
	south	= 180,
	west	= 270
};

typedef struct	s_turtle {
	int			turtle;
	int			step;
	int			n;
	char		tourne;
	direction	d;
	std::string	color;
	std::string	path;
	bool		condition;
	bool		negation;
}				t_turtle;

class JardinRendering;

class Driver {
private:
    JardinRendering * monJardin;
	std::vector<std::string> pileFunc;
	std::vector<t_turtle> pileArg;

public:
    Driver(JardinRendering * J);
    ~Driver();

	void	myError		(const std::string &str);
	int		getTurtle	(const std::string &str);
	bool	outOfMap	(int x, int y);

	void	loadJardin	(t_turtle &t);
	void	addTurtles	(t_turtle &t);
	void	nAvanceN	(t_turtle &t);
	void	nReculeN	(t_turtle &t);
	void	nSauteN		(t_turtle &t);
	void	nTourneN	(t_turtle &t);
	void	nSetCarapace(t_turtle &t);
	void	nSetMotif	(t_turtle &t);
	void	estMur		(t_turtle &t);
	void	estVide		(t_turtle &t);

	void	allAvanceN	(t_turtle &t);
	void	allReculeN	(t_turtle &t);
	void	allSauteN	(t_turtle &t);
	void	allTourneN	(t_turtle &t);
	void	allSetCarapace(t_turtle &t);
	void	allSetMotif	(t_turtle &t);
	void	allEstMur	(t_turtle &t);
	void	allEstVide	(t_turtle &t);

	void	myIf		(t_turtle &t);
	void	myTantQue	(t_turtle &t);
	void	myRepete	(t_turtle &t);

	void	empile		(const std::string &str, t_turtle t);
	void	exec();

private:
	void	execFunction(int j);
	void	depile		();
};
typedef struct  s_pointer {
  std::string	str;
  void			(Driver::*pointer)(t_turtle &t);
}               t_pointer;

// https://misc.flogisoft.com/bash/tip_colors_and_formatting
namespace Color {
	enum Code {
		FG_RED      = 31,
		FG_GREEN    = 32,
		FG_BLUE     = 34,
		FG_DEFAULT  = 39,
		BG_RED      = 41,
		BG_GREEN    = 42,
		BG_BLUE     = 44,
		BG_DEFAULT  = 49
	};
	class Modifier {
		Code code;
	public:
		Modifier(Code pCode) : code(pCode) {}
		friend std::ostream &operator<<(std::ostream &os, const Modifier &mod) {
			return os << "\033[" << mod.code << "m";
		}
	};
}

#endif
