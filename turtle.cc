//
// turtle.cc for  in /home/venatum/rendu/L3-Angers/Compil/Projet
//
// Made by Venatum
// Login   <vincentlequec@gmail.com>
//
// Started on  Mon Nov 19 19:20:07 2018 Venatum
// Last update Mon Nov 19 19:54:47 2018 Venatum
//

#include <list>
#include <algorithm>
#include "turtle.hh"

static const std::list<direction> tabDir = {direction::north, direction::east, direction::south, direction::west};

turtle::turtle(unsigned int x, unsigned int y, direction dir, const std::string &carapace, const std::string &motif) :
        _x(x), _y(y), _dir(dir), _carapace(carapace), _motif(motif) {}

void turtle::avanceN(int n) {
    for (int i = 0; i < n; ++i) {
        switch (_dir) {
            case direction::north :
                _y--;
                break;
            case direction::east :
                _x++;
                break;
            case direction::south :
                _y++;
                break;
            case direction::west :
                _x--;
                break;
        }

    }
}

void turtle::reculeN(int n) {
    for (int i = 0; i < n; ++i) {
        switch (_dir) {
            case direction::north :
                _y++;
                break;
            case direction::east :
                _x--;
                break;
            case direction::south :
                _y--;
                break;
            case direction::west :
                _x++;
                break;
        }

    }
}

void turtle::sauteN(int n) {
    for (int i = 0; i < n; ++i) {
        switch (_dir) {
            case direction::north :
                _y -= 2;
                break;
            case direction::east :
                _x += 2;
                break;
            case direction::south :
                _y += 2;
                break;
            case direction::west :
                _x -= 2;
                break;
        }
    }
}

void turtle::tourneN(char d, int n) {
    auto it(std::find(tabDir.begin(), tabDir.end(), _dir));

    for (int i = 0; i < n; ++i) {
        if (d == 'd') {
            if (it == tabDir.end())
                it = tabDir.begin();
            else
                it++;
        } else {
            if (it == tabDir.begin())
                it = tabDir.end();
            else
                it--;
        }
    }
}

void turtle::setCarapace(const std::string &color) {
    _carapace = color;
}

void turtle::setMotif(const std::string &color) {
    _motif = color;
}
