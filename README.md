# Turtl-GUI

## Project

### Modified files
* driver.cc / .hh
* parser.cc / .hh / .yy
* scanner.ll

### Not Implemented
* Les fonctions
* L'instruction ```jardin "path"``` n'execute rien
* ```tant que``` avec condition infinie

### Implemented
* Toutes les autres instructions
* ```si```
* ```repete```
* ```tant que```

### Problems
* **[SOLVED]** Probleme de tracage avec la fonction ```poserStylo```: lignes commentées - driver.cc - ligne 204, 206, 235 & 237
* Suppression propre des warnings dû aux pointeurs sur fonctions   

### Debug
* Affichage sur clog: driver.cc - ligne 66, 67 & 75 

## Compil
```bash
qmake
make
```

## Tests
* Tester le saut
```shell
 make && ./tortue map/jardin2.txt < example/perso2.turtl
```
* Le reste...
```shell
 make && ./tortue map/jardin1.txt < example/perso.turtl
```

## Authors

* **Vincent Le Quec** - [Venatum](https://github.com/Venatum)

---
