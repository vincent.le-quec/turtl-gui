%skeleton "lalr1.cc"
%require "3.0"

%defines
%define parser_class_name { Parser }
%define api.value.type variant
%define parse.assert

%locations

%code requires{
    class Scanner;
    class Driver;
}

%parse-param { Scanner &scanner }
%parse-param { Driver &driver }

%code{
    #include <iostream>
    #include <string>
    
    #include "scanner.hh"
    #include "driver.hh"

    #undef  yylex
    #define yylex scanner.yylex
}

%token                  NL ADD NEG MUL DIV PO PC EQU LT GT LET GET EQ NEQ AND OR XOR NOT
%token					COMMENTAIRE
%token					FIN SI SINON REPETE TANT_QUE
%token					FONCTION
%token					AVANCE RECULE SAUTE TOURNE COULEUR TORTUES JARDIN FOIS
%token					GAUCHE DROITE DEVANT DERRIERE
%token					PAS MUR VIDE C_GAUCHE C_DROITE C_DEVANT C_DERRIERE C_FOIS
%token <int>            NUMBER
%token <float>          REAL
%token <std::string>    NAME FICHIER HEX_COLOR N_TURTLE C_N_TURTLE


%type <int> nombre
%type <int> operation
%type <bool> condition

%start Input

%left EQU
%left ADD NEG
%left MUL DIV
%left AND OR XOR NOT
%left LT GT LET GET EQ NEQ
%left PO PC

%nonassoc SINON

%%
 // https://www.gnu.org/software/bison/manual/bison.html
Input:
	{ }
	| Input Ligne
	;

Ligne:
	NL			{}
	| COMMENTAIRE NL 	{}
	| Fonction 		{}
	;

Fonction:
	FONCTION NAME Instructions FIN FONCTION {
		driver.exec();
	}
	;

Instructions:
	Instruction					{}
	| Instructions Instruction 	{}
	;

Instruction:
	NL					{}
	| COMMENTAIRE 		{}
	| If_stmt			{}
	| Tq_stmt			{}
	| Rp_stmt			{}
	| deplacement    	{}
	| direction    		{}
	| couleur    		{}
	| TORTUES operation	{
			t_turtle t;
			t.n = $2;
//			driver.addTurtles(t);
			driver.empile("addTurtles", t);
	}
	| JARDIN FICHIER	{
		t_turtle t;
		t.path = $2;
//		driver.loadJardin($2);
		driver.empile("loadJardin", t);
	}
	;

Rp_stmt:
	REPETE operation C_FOIS {
		t_turtle t;
		t.n = $2;
		driver.empile("repete", t);
	}
	| FIN REPETE {
		driver.empile("finRepete", t_turtle());
	}
	;

Tq_stmt:
	TANT_QUE condition		{
		t_turtle t;
		t.condition = $2;
		t.negation = false;
		driver.empile("tantQue", t);
	}
	| TANT_QUE PAS condition		{
		t_turtle t;
		t.condition = !($3);
		t.negation = true;
		driver.empile("tantQue", t);
	}
	| FIN TANT_QUE {
		driver.empile("finTantque", t_turtle());
	}
	;

If_stmt:
	SI condition			{
		t_turtle t;
		t.condition = $2;
		driver.empile("si", t);
	}
	| SI PAS condition 		{
		t_turtle t;
		t.condition = !($3);
		driver.empile("si", t);
	}
	| SINON					{
		driver.empile("sinon", t_turtle());
	}
	| FIN SI				{
		driver.empile("finSi", t_turtle());
	}
	;

condition:
	MUR C_GAUCHE			{
		t_turtle t;
		t.d = direction::west;
		driver.allEstMur(t);
		driver.empile("allEstMur", t);
		$$ = t.condition;
	}
	| MUR C_DROITE			{
		t_turtle t;
		t.d = direction::east;
		driver.allEstMur(t);
		driver.empile("allEstMur", t);
		$$ = t.condition;
	}
	| MUR C_DEVANT			{
		t_turtle t;
		t.d = direction::north;
		driver.allEstMur(t);
		driver.empile("allEstMur", t);
		$$ = t.condition;
	}
	| MUR C_DERRIERE		{
		t_turtle t;
		t.d = direction::south;
		driver.allEstMur(t);
		driver.empile("allEstMur", t);
		$$ = t.condition;
	}
	| VIDE C_GAUCHE			{
		t_turtle t;
		t.d = direction::west;
		driver.allEstVide(t);
		driver.empile("allEstVide", t);
		$$ = t.condition;
	}
	| VIDE C_DROITE			{
		t_turtle t;
		t.d = direction::east;
		driver.allEstVide(t);
		driver.empile("allEstVide", t);
		$$ = t.condition;
	}
	| VIDE C_DEVANT			{
		t_turtle t;
		t.d = direction::north;
		driver.allEstVide(t);
		driver.empile("allEstVide", t);
		$$ = t.condition;
	}
	| VIDE C_DERRIERE		{
		t_turtle t;
		t.d = direction::south;
		driver.allEstVide(t);
		driver.empile("allEstVide", t);
		$$ = t.condition;
	}
	| MUR GAUCHE C_N_TURTLE		{
		t_turtle t;
		t.turtle = driver.getTurtle($3);
		t.d = direction::west;
		driver.estMur(t);
		driver.empile("estMur", t);
		$$ = t.condition;
	}
	| MUR DROITE C_N_TURTLE		{
		t_turtle t;
		t.turtle = driver.getTurtle($3);
		t.d = direction::east;
		driver.estMur(t);
		driver.empile("estMur", t);
		$$ = t.condition;
	}
	| MUR DEVANT C_N_TURTLE	{
		t_turtle t;
		t.turtle = driver.getTurtle($3);
		t.d = direction::north;
		driver.estMur(t);
		driver.empile("estMur", t);
		$$ = t.condition;
	}
	| MUR DERRIERE C_N_TURTLE	{
		t_turtle t;
		t.turtle = driver.getTurtle($3);
		t.d = direction::south;
		driver.estMur(t);
		driver.empile("estMur", t);
		$$ = t.condition;
	}
	| VIDE GAUCHE C_N_TURTLE	{
		t_turtle t;
		t.turtle = driver.getTurtle($3);
		t.d = direction::west;
		driver.estVide(t);
		driver.empile("estVide", t);
		$$ = t.condition;
	}
	| VIDE DROITE C_N_TURTLE	{
		t_turtle t;
		t.turtle = driver.getTurtle($3);
		t.d = direction::east;
		driver.estVide(t);
		driver.empile("estVide", t);
		$$ = t.condition;
	}
	| VIDE DEVANT C_N_TURTLE	{
		t_turtle t;
		t.turtle = driver.getTurtle($3);
		t.d = direction::north;
		driver.estVide(t);
		driver.empile("estVide", t);
		$$ = t.condition;
	}
	| VIDE DERRIERE C_N_TURTLE	{
		t_turtle t;
		t.turtle = driver.getTurtle($3);
		t.d = direction::south;
		driver.estVide(t);
		driver.empile("estVide", t);
		$$ = t.condition;
	}
	;

deplacement:
	AVANCE			{
		t_turtle t;
		t.step = 1;
		driver.empile("allAvanceN", t);
	}
	| AVANCE operation 	{
		t_turtle t;
		t.step = $2;
		driver.empile("allAvanceN", t);
	}
	| AVANCE operation FOIS {
		t_turtle t;
		t.step = $2;
		driver.empile("allAvanceN", t);
	}
	| RECULE 		{
		t_turtle t;
		t.step = 1;
		driver.empile("allReculeN", t);
	}
	| RECULE operation 	{
		t_turtle t;
		t.step = $2;
		driver.empile("allReculeN", t);
	}
	| RECULE operation FOIS {
		t_turtle t;
		t.turtle = -1;
		t.step = $2;
		driver.empile("allReculeN", t);
	}
	| SAUTE  		{
		t_turtle t;
		t.step = 1;
		driver.empile("allSauteN", t);
	}
	| SAUTE operation 	{
		t_turtle t;
		t.step = $2;
		driver.empile("allSauteN", t);
	}
	| SAUTE operation FOIS 	{
		t_turtle t;
		t.step = $2;
		driver.empile("allSauteN", t);
	}
	| AVANCE N_TURTLE			{
		t_turtle t;
		t.turtle = driver.getTurtle($2);
		t.step = 1;
		driver.empile("nAvanceN", t);
	}
	| AVANCE operation N_TURTLE		{
		t_turtle t;
		t.turtle = driver.getTurtle($3);
		t.step = $2;
		driver.empile("nAvanceN", t);
	}
	| AVANCE operation FOIS N_TURTLE	{
		t_turtle t;
		t.turtle = driver.getTurtle($4);
		t.step = $2;
		driver.empile("nAvanceN", t);
	}
	| RECULE N_TURTLE			{
		t_turtle t;
		t.turtle = driver.getTurtle($2);
		t.step = 1;
		driver.empile("nReculeN", t);
	}
	| RECULE operation N_TURTLE		{
		t_turtle t;
		t.turtle = driver.getTurtle($3);
		t.step = $2;
		driver.empile("nReculeN", t);
	}
	| RECULE operation FOIS  N_TURTLE	{
		t_turtle t;
		t.turtle = driver.getTurtle($4);
		t.step = $2;
		driver.empile("nReculeN", t);
	}
	| SAUTE N_TURTLE			{
		t_turtle t;
		t.turtle = driver.getTurtle($2);
		t.step = 1;
		driver.empile("nSauteN", t);
	}
	| SAUTE operation N_TURTLE		{
		t_turtle t;
		t.turtle = driver.getTurtle($3);
		t.step = $2;
		driver.empile("nSauteN", t);
	}
	| SAUTE operation FOIS N_TURTLE		{
		t_turtle t;
		t.turtle = driver.getTurtle($4);
		t.step = $2;
		driver.empile("nSauteN", t);
	}
	;
	
direction:
	TOURNE DROITE				{
		t_turtle t;
		t.tourne = 'd';
		t.n = 1;
		driver.empile("allTourneN", t);
	}
	| TOURNE GAUCHE				{
		t_turtle t;
		t.tourne = 'g';
		t.n = 1;
		driver.empile("allTourneN", t);
	}
	| TOURNE DROITE operation FOIS		{
		t_turtle t;
		t.tourne = 'd';
		t.n = $3;
		driver.empile("allTourneN", t);
	}
	| TOURNE GAUCHE operation FOIS		{
		t_turtle t;
		t.tourne = 'g';
		t.n = $3;
		driver.empile("allTourneN", t);
	}
	| TOURNE DROITE	N_TURTLE		{
		t_turtle t;
		t.turtle = driver.getTurtle($3);
		t.tourne = 'd';
		t.n = 1;
		driver.empile("nTourneN", t);
	}
	| TOURNE GAUCHE N_TURTLE		{
		t_turtle t;
		t.turtle = driver.getTurtle($3);
		t.tourne = 'g';
		t.n = 1;
		driver.empile("nTourneN", t);
	}
	| TOURNE DROITE operation FOIS N_TURTLE	{
		t_turtle t;
		t.turtle = driver.getTurtle($5);
		t.tourne = 'd';
		t.n = $3;
		driver.empile("nTourneN", t);
	}
	| TOURNE GAUCHE operation FOIS N_TURTLE	{
		t_turtle t;
		t.turtle = driver.getTurtle($5);
		t.tourne = 'g';
		t.n = $3;
		driver.empile("nTourneN", t);
	}
	;

couleur:
	COULEUR HEX_COLOR			{
		t_turtle t;
		t.color = $2;
		driver.empile("allSetCarapace", t);
	}
	| COULEUR "carapace" HEX_COLOR		{
		t_turtle t;
		t.color = $3;
		driver.empile("allSetCarapace", t);
	}
	| COULEUR "motif" HEX_COLOR		{
		t_turtle t;
		t.color = $3;
		driver.empile("allSetMotif", t);
	}
	| COULEUR HEX_COLOR N_TURTLE		{
		t_turtle t;
		t.turtle = driver.getTurtle($3);
		t.color = $2;
		driver.empile("nSetCarapace", t);
	}
	| COULEUR "carapace" HEX_COLOR N_TURTLE	{
		t_turtle t;
		t.turtle = driver.getTurtle($4);
		t.color = $3;
		driver.empile("nSetCarapace", t);
	}
	| COULEUR "motif" HEX_COLOR N_TURTLE	{
		t_turtle t;
		t.turtle = driver.getTurtle($4);
		t.color = $3;
		driver.empile("nSetMotif", t);
	}
	;

operation:
	nombre					  {	$$ = $1;		}
	| operation ADD operation {	$$ = $1 + $3;	}
	| operation NEG operation {	$$ = $1 - $3;	}
	| operation MUL operation {	$$ = $1 * $3;	}
	| operation DIV operation {
		if ($3 == 0) {
		  	yy::Parser::error(@3, "[ERROR] Division by zero\n");
		} else {		$$ = $1 / $3;	}
	}
	| NEG operation %prec NEG  {	$$ = -$2;	}
	| NEG PO operation PC 	   {   	$$ = -$3; 	}
	| PO operation PC		   {	$$ = $2;  	}
	| operation LT operation   {	$$ = $1 < $3;  	}
	| operation GT operation   {	$$ = $1 > $3;  	}
	| operation LET operation  {	$$ = $1 <= $3; 	}
	| operation GET operation  {	$$ = $1 >= $3; 	}
	| operation EQ operation   {	$$ = $1 == $3; 	}
	| operation NEQ operation  {	$$ = $1 != $3; 	}
	| operation AND operation  {	$$ = $1 && $3;	}
	| operation OR operation   {	$$ = $1 || $3;	}
	| operation XOR operation  {	$$ = ($1 || $3) && !($1 && $3);}
	| NOT operation			   {	$$ = !$2;	}
	;

 // mettre and or ... ds un autre truc

nombre:
	NUMBER			{	$$ = $1;    	}
	;
    
%%

void yy::Parser::error( const location_type &l, const std::string & err_msg) {
    std::cerr << "Erreur : " << l << ", " << err_msg << std::endl;
//    exit(1);
}
