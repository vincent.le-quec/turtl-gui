//
// turtle.hh for Turtle Header in /home/venatum/rendu/L3-Angers/Compil/Projet
//
// Made by Venatum
// Login   <vincentlequec@gmail.com>
//
// Started on  Mon Nov 19 19:16:23 2018 Venatum
// Last update Mon Nov 19 19:16:25 2018 Venatum
//

#ifndef PROJET_TURTLE_HH
#define PROJET_TURTLE_HH

#include <string>

enum class direction {
    north,
    east,
    south,
    west
};

class turtle {
public:
    turtle(unsigned int x, unsigned int y, direction dir, const std::string &carapace, const std::string &motif);

    void    avanceN(int n);
    void    reculeN(int n);
    void    sauteN(int n);
    void    tourneN(char d, int n);

    void    setCarapace(const std::string &color);
    void    setMotif(const std::string &color);

private:
    unsigned int    _x;
    unsigned int    _y;
    direction       _dir;
    std::string     _carapace;
    std::string     _motif;
};

#endif //PROJET_TURTLE_HH
