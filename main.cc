//
// main.cc for  in /home/venatum/rendu/L3-Angers/Compil/Projet
//
// Made by Venatum
// Login   <vincentlequec@gmail.com>
//
// Started on  Wed Nov 28 13:06:19 2018 Venatum
// Last update Sat Dec  1 15:24:19 2018 Venatum
//

#include "parser.hh"
#include "scanner.hh"
#include "driver.hh"
#include "jardin.hh"

#include <iostream>
#include <fstream>

#include <cstring>

#include <QtGui>
#include <QApplication>


int main( int  argc, char* argv[]) {
    if (argc > 2){
        std::cerr << "Trop d'arguments, format attendu ./tortue *jardin* < programme " << std::endl;
        return 0;
    } else{
        Jardin		* J;
        QApplication	app(argc,argv);

        if (argc == 2)
            J = new Jardin(argv[1]);
        else
            J = new Jardin;
        J->show();
        return app.exec();
    }

}
